#
# School for Ninja
# Library where actions on the system are defined. These actions are stored on the "activity log"
# (c)2018 School for Ninja BV

from actstream import action

NINJA_ACTION = [(
    ('register'),
    ('login'),
    ('start_quest'),
    ('complete_quest'),
    ('logout'),
    ('phase_start'),
    ('add_coach'),
    ('remove_coach'),
    ('create_post'),
    ('delete_post'),
    ('edit_post'),
    ('create_quest'),
    ('edit_quest'),
    ('remove_quest'),
    ('activate_quest'),
    ('deactivate_quest'),
    ('send_message'),
    ('receive_message'),
    ('rate_quest'),
    ('report_user'),
    ('register_achievement'),
    ('create_profile'),
    ('edit_profile'),
    ('delete_profile'),
    ('unlock_quest'),
    ('change_profile_privacy'),
    ('add_draft'),
    ('edit_draft'),
    ('remove_draft'),
    ('report_issue'),
    ('login_error'),
    ('login_password_reset'),
    )]
