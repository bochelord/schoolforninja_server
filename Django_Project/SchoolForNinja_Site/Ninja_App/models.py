from django.db import models
from django.contrib.auth.models import User

from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill, ResizeToFit

class Coach(models.Model):

    GENDER = (
        ('m', 'male'),
        ('f', 'female'),
        ('o', 'other')
        )

    user = models.OneToOneField(User, primary_key=True, on_delete=models.CASCADE)
    about_me = models.TextField(blank=True, null=True)
    gender = models.CharField(max_length=1, choices=GENDER)
    is_in_organization = models.ForeignKey('Organization', on_delete=models.CASCADE) #Remember: if you delete Organization all the Coaches in that Organization will be also deleted.

    def __str__(self):
        return str(self.user.id)+": "+self.user.username

#Organization has to be a kind of user also!!!!
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
class Organization(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField(blank=True, null=True)
    has_coaches = models.ForeignKey(Coach, blank=True, null=True, on_delete=models.SET_NULL) 
    website = models.CharField(max_length=300)
    
    def __str__(self):
        return str(self.id)+": "+self.name

class Ninja(models.Model):

    GENDER = (
        ('m', 'male'),
        ('f', 'female'),
        ('o', 'other')
        )

    PRIVACY = (
        ('everyone', 'Everyone'),
        ('friends', 'Friends'),
        ('group_members', 'Group members'),
        ('friends_and_group', 'Friends and Group members'),
        ('only_me', 'Only me')
        )

    # role = Student, Teacher
    user = models.OneToOneField(User, primary_key=True, on_delete=models.CASCADE)
    ninja_name = models.CharField(max_length=100)
    avatar = models.ImageField(upload_to="images/avatars",default="default_images/default_avatar.png")
    avatar_big = ImageSpecField(source='image',
                                     processors=[ResizeToFit(355, 525)],
                                     format='PNG',
                                     options={'quality': 100})
    avatar_thumbnail = ImageSpecField(source='image',
                                     processors=[ResizeToFit(243, 243)],
                                     format='PNG',
                                     options={'quality': 100})    
    gender = models.CharField(max_length=1, choices=GENDER)
    level = models.IntegerField(default=0)
    domain = models.CharField(max_length=100)
    power = models.CharField(max_length=100)
    profile_privacy = models.CharField(max_length=20, choices=PRIVACY)
    has_friends = models.ForeignKey('Ninja',null=True, blank=True, on_delete=models.SET_NULL)
    has_coaches = models.ForeignKey('Coach',null=True, blank=True, on_delete=models.SET_NULL)

    def __str__(self):
        return str(self.user.id)+": "+self.ninja_name

class Quest(models.Model):

    image = models.ImageField(upload_to="images/quests",default="default_images/default_quest.png")
    title_text = models.CharField(max_length=500)
    description = models.TextField()
    creation_date = models.DateTimeField('date created')
    duration = models.DateTimeField()

    #def admin_image(self):
    #    if self.image:
    #        return '<img src="%s" width="100"  />' % self.image.url
    #    else:
    #        return 'Sin Imagen'
    #    admin_image.short_description = 'Image'
    #    admin_image.allow_tags = True

    def image_tag(self):
        return '<img src="%s" />' % self.image.url
    image_tag.short_description = 'Image'
    image_tag.allow_tags = True

    def __str__(self):
        return str(self.id)+": "+self.title_text


class Quest_Status(models.Model):
    ninja = models.ForeignKey('Ninja', null=False, on_delete=models.CASCADE) # ForeignKey is Many to One
    quest = models.ForeignKey('Quest', null=False, on_delete=models.CASCADE)
    started_on_date = models.DateTimeField()
    completed_on_date = models.DateTimeField(blank=True, null=True)
    unlocked = models.BooleanField(default=True)
    completed = models.BooleanField(default=False)

    def __str__(self):
        return self.ninja.ninja_name + " - " + self.quest.title_text +" - "+  str(self.quest.id)


