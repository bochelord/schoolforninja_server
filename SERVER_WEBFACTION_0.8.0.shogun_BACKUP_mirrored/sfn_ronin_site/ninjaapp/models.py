from django.db import models
from django.contrib.auth.models import User
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill
from django.contrib.postgres.fields import ArrayField


class Coach(models.Model):

    GENDER = (
        ('m', 'male'),
        ('f', 'female'),
        ('o', 'other')
        )

    user = models.OneToOneField(User, primary_key=True, on_delete=models.CASCADE)
    coach_name = models.CharField(max_length=250, default="Elbert-Jan")
    coach_avatar = models.ImageField(upload_to="images/avatars",default="default_images/default_avatar.png")
    about_me = models.TextField(blank=True, null=True)
    gender = models.CharField(max_length=1, choices=GENDER)
    is_in_organization = models.ForeignKey('Organization',null=True, blank=True, on_delete=models.CASCADE) #Remember: if you delete Organization all the Coaches in that Organization will be also deleted?
    has_ninjas = models.ManyToManyField('Ninja', blank=True)

    def __str__(self):
        return str(self.user.id)+": "+self.coach_name

#Organization has to be a kind of user also!!!!
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
class Organization(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField(blank=True, null=True)
    has_coaches = models.ForeignKey(Coach, blank=True, null=True, on_delete=models.SET_NULL) 
    website = models.CharField(max_length=300)
    
    def __str__(self):
        return str(self.id)+": "+self.name

class Organization_User(models.Model):
    user = models.OneToOneField(User, primary_key=True, on_delete=models.CASCADE)
    is_in_organization = models.ForeignKey('Organization', on_delete=models.CASCADE) #Remember: if you delete Organization all the Org_Users in that Organization will be also deleted.

class Ninja(models.Model):

    GENDER = (
        ('m', 'male'),
        ('f', 'female'),
        ('o', 'other')
        )

    PRIVACY = (
        ('everyone', 'Everyone'),
        ('friends', 'Friends'),
        ('group_members', 'Group members'),
        ('friends_and_group', 'Friends and Group members'),
        ('only_me', 'Only me')
        )

    # role = Student, Teacher
    user = models.OneToOneField(User, primary_key=True, on_delete=models.CASCADE)
    ninja_name = models.CharField(max_length=100, default="ninja")
    avatar = models.ImageField(upload_to="images/avatars",default="default_images/default_avatar.png")
    gender = models.CharField(max_length=1, choices=GENDER)
    level = models.IntegerField(default=0)
    domain = models.CharField(max_length=100)
    power = models.CharField(max_length=100)
    # profile_privacy = models.CharField(max_length=20, choices=PRIVACY)
    has_friends = models.ManyToManyField('Ninja', blank=True)
    has_coaches = models.ManyToManyField('Coach', blank=True)
    has_quests = models.ManyToManyField('Quest', blank=True)
    has_badges = models.ManyToManyField('Badge', blank=True)
    first_time_connected = models.BooleanField(default=True)
    
    def update_user_profile(sender,instance,created,**kwargs):
        if created:
            Ninja.objects.create(user=instance)
        instance.ninja.save()

    def __str__(self):
        return self.ninja_name

class Quest_Page(models.Model):
    order = models.IntegerField(default=0)
    quest_title = models.CharField(max_length=100, blank=True)
    has_image = models.BooleanField(default=False)
    title_text = models.CharField(max_length=500, blank=True)
    is_info_page = models.BooleanField(default=False)
    description_text = models.TextField(null=False, blank=True, default="default text")
    question_text = models.TextField(null=False,default="default question")
    image = models.ImageField(upload_to="images/quests",default="default_images/default_quest.png")
    belongs_to_quest = models.ForeignKey('Quest', null=True)
    def __str__(self):
        return str(self.id) + ' ' + self.quest_title

class Quest(models.Model):
    quest_title = models.CharField(max_length=100,blank=True)
    creation_date = models.DateTimeField('date created',auto_now_add=True)
    duration = models.DateField()
    user_generated = models.BooleanField(default=False)
    is_from_phase = models.ForeignKey('Phase', null=True)
    is_approved = models.BooleanField(default=False)
    #has_pages = models.ManyToManyField('Quest_Page', related_name='pages')
    organization = models.ForeignKey('Organization', null=True)
    def __str__(self):
        return str(self.id) + " - " + self.quest_title




class Quest_Status(models.Model):
    belongs_to_ninja = models.ForeignKey('Ninja', null=False, on_delete=models.CASCADE) # ForeignKey is Many to One
    belongs_to_quest = models.ForeignKey('Quest', null=False, on_delete=models.CASCADE)
    started_on_date = models.DateTimeField(auto_now_add=True)
    completed_on_date = models.DateTimeField(blank=True, null=True)
    quest_page_active = models.IntegerField(default=0)
    #unlocked = models.BooleanField(default=False)
    completed = models.BooleanField(default=False)
    #started = models.BooleanField(default=False)
    has_drafts = models.ManyToManyField('Draft', related_name='drafts',blank=True)
    def __str__(self):
        return self.belongs_to_ninja.ninja_name + " in quest: " + self.belongs_to_quest.quest_title +" - "+  str(self.belongs_to_quest.id)


class Post_Log(models.Model):
    PRIVACY = (
        ('everyone', 'Everyone'),
        ('friends', 'Friends'),
        ('group_members', 'Group members'),
        ('friends_and_group', 'Friends and Group members'),
        ('only_me', 'Only me')
    )
    postTitle = models.CharField(max_length=50,null=False, blank=False, default='Title')
    postContent1 = models.TextField(null=False, blank=True)
    postContent2 = models.TextField(null=False, blank=True)
    postContent3 = models.TextField(null=False, blank=True)
    postContent4 = models.TextField(null=False, blank=True)
    postContent5 = models.TextField(null=False, blank=True)
    has_image1 = models.BooleanField(default=False)
    has_image2 = models.BooleanField(default=False)
    has_image3 = models.BooleanField(default=False)
    has_image4 = models.BooleanField(default=False)
    has_image5 = models.BooleanField(default=False)
    image1 = models.ImageField(null=True,upload_to="images/posts",default="default_images/default_badge.jpg")
    image2 = models.ImageField(null=True,upload_to="images/posts",default="default_images/default_badge.jpg")
    image3 = models.ImageField(null=True,upload_to="images/posts",default="default_images/default_badge.jpg")
    image4 = models.ImageField(null=True,upload_to="images/posts",default="default_images/default_badge.jpg")
    image5 = models.ImageField(null=True,upload_to="images/posts",default="default_images/default_badge.jpg")
    belongs_to_ninja = models.ForeignKey('Ninja', null=False, on_delete=models.CASCADE)
    belongs_to_quest = models.ForeignKey('Quest', null=False, on_delete=models.CASCADE)
    timestamp = models.DateTimeField(auto_now_add=True)
    privacy = models.CharField(max_length=20, choices=PRIVACY)

    def __str__(self):
        return  self.postTitle

class Badge(models.Model):

    name = models.CharField(max_length=250)
    description = models.TextField()
    image = models.ImageField(upload_to="images/badges",default="default_images/default_badge.jpg")
    image_thumbnail = ImageSpecField(source='image',
                                      processors=[ResizeToFill(50, 50)],
                                      format='JPEG',
                                      options={'quality': 100})
    unlocked_date = models.DateTimeField('Unlocked Date', blank=True, null=True)

    def __str__(self):
        return self.name


class Phase(models.Model):

    name = models.CharField(max_length=150,null=False,blank=False)
    theme = models.TextField(max_length=250, blank=False)
    info = models.TextField(max_length=500,blank=False)

    def __str__(self):
        return self.name

class Draft(models.Model):
    has_image = models.BooleanField(default=False)
    image = models.ImageField(null=True,upload_to="images/drafts",default="default_images/default_badge.jpg")
    description = models.TextField(max_length=500,blank=False)
    belongs_to_ninja = models.ForeignKey('Ninja',null=True, on_delete=models.CASCADE)
    belongs_to_quest = models.ForeignKey('Quest',null=True, on_delete=models.CASCADE)


class SurveyQuestion(models.Model):
    order_in_survey = models.IntegerField()
    question_title = models.CharField(max_length = 150)
    question_text = models.TextField(max_length = 500)
    question_answers = ArrayField(models.CharField(max_length=100))
    end_block = models.BooleanField(default=False)
    def __str__(self):
        return str(self.order_in_survey) + " " + self.question_title

class SurveyAnswers(models.Model):
    belongs_to_ninja = models.ForeignKey('Ninja')
    answers = ArrayField(models.IntegerField())
    def __str__(self):
        return str(self.belongs_to_ninja.ninja_name + " survey answers")
