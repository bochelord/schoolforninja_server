from django.shortcuts import render, redirect, render_to_response
import json
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view, permission_classes
from rest_framework import permissions
from rest_framework.permissions import AllowAny
from ninjaapp.models import ServerStatus, Phase_Status,ChatHistory,Draft,Ninja, Post_Log, Coach, Organization, Quest, Quest_Status, Badge, Phase, Quest_Page,SurveyAnswers,SurveyQuestion
from imagekit.models import ImageSpecField
from imagekit import ImageSpec, register
from imagekit import processors
from imagekit.processors import ResizeToFill
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm, PasswordResetForm
from ninjaapp.forms import SignUpForm, SignUpNinjaForm, SignUpCoach
from ninjaapp.ninjaapp_imagekit import *
from django.contrib.auth.models import User
from django.conf import settings

from django.views import View
from django.views.generic.base import TemplateView
import sys
import os

import requests
from actstream.models import user_stream
from actstream import action
from actstream.models import Action

from django.shortcuts import get_object_or_404

#imports for NINJAPI
from rest_framework.response import Response
from rest_framework import viewsets
from ninjaapp.serializers import ServerStatusSerializer,Phase_StatusSerializer,ChatHistorySerializer,SurveyQuestionSerializer,SurveyAnswersSerializer,DraftSerializer,Quest_PageSerializer,UserSerializer, NinjaSerializer, CoachSerializer, OrganizationSerializer, QuestSerializer, Quest_StatusSerializer, Post_LogSerializer, BadgeSerializer, PhaseSerializer, ActionSerializer



# def index(request):

#     ninjas = Ninja.objects.all()
    
    
#     print('CABEZAAAA' + settings.SCHOOLFORNINJA_VERSION)

#     context = { 'ninjas': ninjas,
#                 'SCHOOLFORNINJA_VERSION': settings.SCHOOLFORNINJA_VERSION }
#     return render(request, 'index.html', context)



# def password_reset(email, from_email, template='registration/password_reset_email.html'):
@csrf_exempt
@api_view(['POST'])
@permission_classes((permissions.AllowAny,))
def password_reset(request, template='emails/password_reset_email.html'):
    """
    Reset the password for all (active) users with given E-Mail adress
    """
    data=json.loads(request.body)
    model = User
    email=data['email']
    from_email=data['from_email']
    pagination_class = None
    message = None
    explanation = None
    status_code = 500

    def handle_exception(message, explanation, status_code):
        responseData = {
            'message': message,
            'explanation': explanation,
        }
        return HttpResponse(json.dumps(responseData), content_type="application/json", status=status_code)
    
    try:
        match = User.objects.get(email=email)
        if match:
            form = PasswordResetForm({'email': email})
            if form.is_valid():
                form.save(from_email=from_email, email_template_name=template, subject_template_name='registration/password_reset_subject.txt')
                print("email has been accepted, all ok <<<<<<<<<<<<<<<<<<<<<<<<<")
                return HttpResponse("Email is valid!")
                
            else:
                print("form is not valid, else statement <<<<<<<<<<<<<<<<<<<<<<<<<")
                message = "Email doesn't exists"
                explanation = "This user email doesn't exists in the database"
                status_code = 400
                handle_exception(message, explanation, status_code)
    except:
        print("except has been triggered! <<<<<<<<<<<<<<<<<<<<<<<<<")
        message = "User doesn't match"
        explanation = "The user email doesn't exists in the database"
        status_code = 400
    
        responseData = {
            'message': message,
            'explanation': explanation,
        }
    
        print(json.dumps(responseData) + " status code: " + str(status_code))
        
        return HttpResponse(json.dumps(responseData), content_type="application/json", status=status_code)
    



# ==================================
# HTML page to test Codrops elements
# ==================================
def index_test(request):
    pagination_class = None
    context = { 'user' : request.user }

    return render(request, 'index-test.html', context)



def test_actions(request):
    pagination_class = None
    if request.user.is_authenticated:
        action.send(request.user, verb='action_test', description='Welcome to Ninjaapp!', shown='no')
        return HttpResponse("Action_test created on the DB, you should check it out on the admin...")
    else:
        return HttpResponse("You are not logged in")


def signup(request):
    pagination_class = None
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        ninjaform = SignUpNinjaForm(request.POST)
        if all([form.is_valid() and ninjaform.is_valid()]):
            user = form.save()
            ninja = ninjaform.save(False)
            ninja.user = user
            ninjaform.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            email = form.cleaned_data.get("email")
            user = authenticate(username=username, email=email, password=raw_password)
            login(request, user)
            return redirect('index')
    else:
        form = SignUpForm()
        ninjaform = SignUpNinjaForm()
    return render(request, 'signup.html', {'form': form , 'ninjaform':ninjaform })

def signupcoach(request):
    pagination_class = None
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        coachform = SignUpCoach(request.POST)
        if all([form.is_valid() and coachform.is_valid()]):
            user = form.save()
            coach = coachform.save(False)
            coach.user = user
            coachform.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            email = form.cleaned_data.get("email")
            user = authenticate(username=username, email=email, password=raw_password)
            login(request, user)
            return redirect('index')
    else:
        form = SignUpForm()
        coachform = SignUpCoach()
    return render(request, 'signupcoach.html', {'form': form , 'coachform':coachform })




class NinjaView(View):
    pagination_class = None
    def get(self,request):
        return HttpResponse(Ninja.objects.all())

class SfN_IndexView(TemplateView):
    pagination_class = None
    
    def get(sef,request):
        ninjas = Ninja.objects.all()
        
        print(' ')
        print(' ')
        print('INDEX SfN :: ' + settings.SCHOOLFORNINJA_VERSION )
        print(' ')
        print(' ')

        context = { 'ninjas': ninjas,
                    'SCHOOLFORNINJA_VERSION': settings.SCHOOLFORNINJA_VERSION }
        return render(request, 'index.html', context)



def debug(request):

    print("BASE_DIR: " + settings.BASE_DIR + "   <<<<<<<<<<<<<<<<<")
    print("SITE_ROOT: " + settings.SITE_ROOT + "   <<<<<<<<<<<<<<<<<")
    return HttpResponse("MIRA TU CONSOLA AMIGOOO")





def profile(request):
    pagination_class = None
    if request.user.is_authenticated():

        local_ninja = get_object_or_404(Ninja,user_id=request.user.id)
        local_friends = Ninja.objects.filter(has_friends=request.user.id)
        local_logs = Post_Log.objects.filter(belongs_to_ninja=request.user.id)
        print(local_ninja)
        print(local_friends)
        print(local_logs)
        #sys.exit()

        context = {'ninja':local_ninja,
                   'friends':local_friends,
                   
                   }

        return render(request,'profile.html',context)

    else:
        return HttpResponseRedirect('index')






#def SignupViewSet(viewsets.ModelViewSet):

#    serializer_class = UserSerializer
    
#    def post(self, request, format=None):

#        serialized = UserSerializer(data=request.data)
#        if serialized.is_valid():
#            serialized.save()
#            return Response(serialized.data, status=status.HTTP_201_CREATED)
#        else:
#            return Response(serialized._errors, status=status.HTTP_400_BAD_REQUEST)
@csrf_exempt
@api_view(['POST'])
@permission_classes((permissions.AllowAny,))
def signup_user(request):

    queryset = User.objects.all()
    serializer = UserSerializer(data=request.data, context={'request': request})
    pagination_class = None
    if serializer.is_valid():
       # User.objects.create(
       #     serializer.data['email'],
       #     serializer.data['username'],
       #     serializer.data['password']
       # )
       serializer.save()
       #return Response(serializer.data, status=status.HTTP_201_CREATED)
       return Response(serializer.data)
    else:
#        return Response(serializer._errors, status=status.HTTP_400_BAD_REQUEST)
        return Response(serializer._errors)

#############
##### NINJAPI
#############
# We have imported already the serializers for the API representation

class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
    pagination_class = None
#    @permission_classes((AllowAny, ))
 #   def post(self,request,format=None):
  #      serializer = UserSerializer(data=request.data)
   #     if serializer.is_valid():
    #        serializer.save()
     #       return Response(serializer.data)
      #  else:
       #     return Response(serializer.errors)

class NinjaViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows ninjas to be viewed or edited.
    """
    permission_classes = (AllowAny,)
    queryset = Ninja.objects.all()
    serializer_class = NinjaSerializer
    filter_fields = ('user','has_coaches','ninja_name')
    pagination_class = None
    def put(self,request,pk,format=None):
        if request.user.is_authenticated():
            ninja = Ninja.objects.filter(user_stream=request.user)
            searchNinja = Ninja.get_object(pk)
            if ninja.user == searchNinja.user:
                serializer = NinjaSerializer(searchNinja,data=request.data)
                if serializer.is_valid():
                    serializer.save()
                    return Response(serializer.data)
                else:
                    return Response(serializer.errors)
    # def post(self,request,format=None):
    #     serializer = NinjaSerializer(data=request.data)
    #     if serializer.is_valid():
    #         serializer.save()
    #         return Response(serializer.data)
    #     else:
    #         return Response(serializer.errors)

class CoachViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows coaches to be viewed or edited.
    """
    queryset = Coach.objects.all()
    serializer_class = CoachSerializer
    filter_fields = ('user',)
    pagination_class = None

class OrganizationViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows organizations to be viewed or edited.
    """
    queryset = Organization.objects.all()
    serializer_class = OrganizationSerializer
    pagination_class = None

class QuestPagesViewSet(viewsets.ModelViewSet):
    queryset = Quest_Page.objects.all()
    serializer_class = Quest_PageSerializer
    filter_fields = ('belongs_to_quest',)
    pagination_class = None

class QuestViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows quests to be viewed or edited.
    """
    queryset = Quest.objects.all()
    serializer_class = QuestSerializer
    filter_fields = ('is_from_phase',)
    pagination_class = None

    def put(self,request,pk,format=None):
        quest = Quest.get_object(pk)
        serializer = QuestSerializer(quest,data=request.data, many=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors)

    def post(self,request,format=None):
        serializer = QuestSerializer(data=request.data,many=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors)

class DraftViewSet(viewsets.ModelViewSet):
    queryset = Draft.objects.all()
    serializer_class = DraftSerializer
    filter_fields = ('belongs_to_ninja', 'belongs_to_quest')
    pagination_class = None


class Quest_StatusViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows quest_status to be viewed or edited.
    """
    queryset = Quest_Status.objects.all()
    serializer_class = Quest_StatusSerializer
    filter_fields = ('belongs_to_ninja','belongs_to_quest',)
    pagination_class = None

    def put(self,request,pk,format=None):
        quest_status = Quest_Status.get_object(pk)
        serializer = Quest_StatusSerializer(quest_status,data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors)

    def post(self,request,format=None):
        serializer = Quest_StatusSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors)


class Post_LogViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows post_logs to be viewed or edited.
    """
    queryset= Post_Log.objects.all()
    serializer_class = Post_LogSerializer
    filter_fields = ('belongs_to_ninja', 'belongs_to_quest','privacy')
    pagination_class = None

    def put(self,request,pk,format=None):
        if request.user.is_authenticated():
            ninja = Ninja.objects.filter(user_stream=request.user)
            post = Post_Log.get_object(pk)
            if ninja == post.belongs_to_ninja:
                serializer = Post_LogSerializer(post,data=request.data)
                if serializer.is_valid():
                    serializer.save()
                    return Response(serializer.data)
                else:
                    return Response(serializer.errors)

    def post(self,request,format=None):
        if request.user.is_authenticated():
            serializer = Post_LogSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            else:
                return Response(serializer.errors)

class BadgeViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows badges to be viewed or edited.
    """
    queryset = Badge.objects.all()
    serializer_class = BadgeSerializer
    pagination_class = None

class Phase_StatusViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows quest_status to be viewed or edited.
    """
    queryset = Phase_Status.objects.all()
    serializer_class = Phase_StatusSerializer
    filter_fields = ('belongs_to_ninja','belongs_to_phase','completed')
    pagination_class = None

    def put(self,request,pk,format=None):
        phase_status = Phase_Status.get_object(pk)
        serializer = Phase_StatusSerializer(phase_status,data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors)

    def post(self,request,format=None):
        serializer = Phase_StatusSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors)


class PhaseViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows phases to be viewed or edited.
    """
    queryset = Phase.objects.all()
    serializer_class = PhaseSerializer    
    pagination_class = None

class ServerStatusViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows phases to be viewed or edited.
    """
    permission_classes = (AllowAny,)
    queryset = ServerStatus.objects.all()
    serializer_class = ServerStatusSerializer    
    pagination_class = None

class ActionViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows actions to be viewed.
    """
    queryset = Action.objects.all()
    serializer_class = ActionSerializer
    filter_fields = ('actor_object_id','verb', 'target_object_id')
    pagination_class = None
class SurveyQuestionViewSet(viewsets.ModelViewSet):

    queryset = SurveyQuestion.objects.all()
    serializer_class = SurveyQuestionSerializer
    pagination_class = None

class ChatHistoryViewSet(viewsets.ModelViewSet):
    queryset = ChatHistory.objects.all()
    serializer_class = ChatHistorySerializer
    filter_fields = ('sender','channel',)

class SurveyAnswersViewSet(viewsets.ModelViewSet):
    queryset= SurveyAnswers.objects.all()
    serializer_class = SurveyAnswersSerializer
    filter_fields = ('belongs_to_ninja',)
    pagination_class = None
    
    def post(self,request,format=None):
        if request.user.is_authenticated():
            serializer = SurveyAnswersSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            else:
                return Response(serializer.errors)


register.generator('ninjaapp:thumbnail30x30', Thumbnail30x30)
register.generator('ninjaapp:thumbnail50x50', Thumbnail50x50)


