from .base import *

ALLOWED_HOSTS = ['127.0.0.1', 'localhost']

DEBUG = True


# STATIC_ROOT = 'D:/Github/schoolforninja_server/webfaction_schoolforninja/sfn_ronin_site/static/'

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.sqlite3', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
#         'NAME': os.path.join(SITE_ROOT, 'db.sqlite3'),   # Or path to database file if using sqlite3.
#     }
# }
STATIC_ROOT = '/sfn_ronin_static/static/'
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
   'D:/Github/schoolforninja_server/webfaction_schoolforninja/sfn_ronin_site/static/',
   '/ninjaapp/css',
    #os.path.join(os.path.dirname(__file__),'media').replace('\\','/'),
)



DATABASES = {
    'default': {
        'CONN_MAX_AGE': 0,
        'ENGINE': 'django.db.backends.postgresql_psycopg2', 
        'NAME': 'postgres',
        'USER': 'postgres',
        'PASSWORD': 'ksironline.com1',
        'HOST': '127.0.0.1',   # Or an IP Address that your DB is hosted on
        'PORT': '5432',
    }
}

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

EMAIL_HOST='smtp.webfaction.com'
EMAIL_HOST_USER='schoolforninja'
EMAIL_HOST_PASSWORD='ksironline.com1'
EMAIL_PORT = 587
