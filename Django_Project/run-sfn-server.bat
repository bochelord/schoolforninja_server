cls
@echo off
echo ------------------------------
echo ------------------------------
echo School for Ninja v0.2 Kunoichi
echo ©2018 School for Ninja BV
echo ==============================
echo ==============================
echo SfN server initializing....
echo -------------------------------
call env_tatami/Scripts/activate
echo Virtual env_tatami activated...
REM Weird way of waiting, pinging a reserved nonexistant IP  - as detailed in a RFC -  by default and wait a timeout of 500 miliseconds 
ping 192.0.2.2 -n 1 -w 500 > nul


REM We run the server
python sfn_tatami_site/manage.py runserver 127.0.0.1:8001