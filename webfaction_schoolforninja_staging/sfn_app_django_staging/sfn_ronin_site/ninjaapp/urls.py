# URLS.PY Router for URLs on Ninja App
#
#


from django.conf.urls import url
from ninjaapp import views
#from django.views.generic.base import TemplateView

urlpatterns = [
	url(r'^testaction/', views.test_actions, name='test_actions'),
    url(r'^classbasedview/', views.NinjaView.as_view()),
    url(r'^debug/', views.debug, name='debug'),
    #url(r'^$', TemplateView.as_view(template_name='index.html'), name='index'),
    url(r'^$', views.SfN_IndexView.as_view(template_name='index.html'), name='index'),
    url(r'^signup/$', views.signup, name='signup'),
    url(r'^signupcoach/$', views.signupcoach, name='signupcoach'),
    url(r'^profile/$', views.profile, name='profile'),
    url(r'^test-codrops/$', views.index_test, name='index_test')
   # url(r'^account_activation_sent/$', views.account_activation_sent, name='account_activation_sent'),
    #url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        #views.activate, name='activate'),
    ]