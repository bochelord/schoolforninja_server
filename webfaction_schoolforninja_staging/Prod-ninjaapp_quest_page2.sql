--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Data for Name: ninjaapp_quest_page; Type: TABLE DATA; Schema: public; Owner: radicalg
--

INSERT INTO public.ninjaapp_quest_page VALUES (12, 'Hoe gaat het?', false, 'Hoe gaat het?', 'Luister nu eens naar jouw onderbuik gevoel. Hoe voel je je, in het hier en nu? 

Vul je vorige antwoord aan vanuit je gevoel.', 'default_images/default_quest.png', 10, false, 'Hoe gaat het met jou?', 2, false);
INSERT INTO public.ninjaapp_quest_page VALUES (13, 'Hoe gaat het?', false, 'Hoe gaat het?', 'https://www.youtube.com/watch?v=sEAOZPejbL0&feature=youtu.be', 'default_images/default_quest.png', 10, false, 'Is er verschil tussen jouw eerste en tweede antwoord? Wil je nog iets kwijt?', 3, true);
INSERT INTO public.ninjaapp_quest_page VALUES (15, 'Jouw Ninja Naam', false, 'Jouw Ninja Naam', 'Je gaat in deze quest spelenderwijs in stappen jouw Ninja Naam vinden.

<b> Stap 1:</b> Doe een minuut lang je ogen dicht en denk aan een leuke eigenschap van jezelf.', 'default_images/default_quest.png', 11, false, '<i> Aan welke eigenschap dacht je? </i>', 1, false);
INSERT INTO public.ninjaapp_quest_page VALUES (17, 'Jouw Ninja Naam', false, 'Jouw Ninja Naam', 'Stap 3: Combineer nu stap 1 (jouw leuke eigenschap) met stap 2 (het antwoord op jouw vraag) op een manier die jij leuk vindt en plaats het woord Ninja ervoor.', 'default_images/default_quest.png', 11, false, '<i>Wat is jouw Ninja naam?</i>', 3, false);
INSERT INTO public.ninjaapp_quest_page VALUES (19, 'Stoppen of doorrijden?', false, 'Stoppen of doorrijden?', 'Stel, je staat voor een rood stoplicht. Je kijkt om je heen, er is geen mens of voertuig te bekennen. Het stoplicht staat nog steeds op rood. Wat doe je?', 'default_images/default_quest.png', 13, false, 'Blijf je staan? Of rijd je verder?', 1, false);
INSERT INTO public.ninjaapp_quest_page VALUES (20, 'Stoppen of doorrijden?', false, 'Stoppen of doorrijden?', 'Het antwoord dat je op de vorige vraag hebt gegeven, zegt iets over jouw manier van omgaan met regels.

Als je ''stoppen'' hebt geantwoord, zou dit kunnen impliceren dat je je graag aan regels houdt, ook wanneer de situatie er misschien niet bij past. 

Zie het stoplicht nu eens als symbool voor een van jouw ''leefregels''. Dit kan een regel zijn die je jezelf oplegt of een regel die iemand anders je heeft gegeven.', 'default_images/default_quest.png', 13, false, 'Aan welke leefregel denk je dan?', 2, false);
INSERT INTO public.ninjaapp_quest_page VALUES (21, 'Stoppen of doorrijden?', false, 'Stoppen of doorrijden?', 'Net als met het stoplicht, ga je ook op een bepaalde manier met jouw leefregels om. 

Houd je je vast aan je regels, ook als dat in een bepaalde situatie niet nodig of wenselijk is? Of kun je zo''n regel dan loslaten en je gedrag aanpassen aan de situatie? Welk effect heeft dat op jou denk je?', 'default_images/default_quest.png', 13, false, 'Vertel!', 3, false);
INSERT INTO public.ninjaapp_quest_page VALUES (23, 'Donuts', true, 'Donuts', '', 'images/quests/Schermafbeelding_2019-01-31_om_14.10.27.png', 14, false, 'Bekijk deze afbeelding, je mag er ook meer informatie over opzoeken.
Waar gaat deze afbeelding volgens jou over?', 1, false);
INSERT INTO public.ninjaapp_quest_page VALUES (24, 'Donuts', true, 'Donuts', '', 'images/quests/Learning_zones.png', 14, false, 'Bekijk nu deze afbeelding. 
In welke zone zit jij? Wil je daar ook zijn?', 2, false);
INSERT INTO public.ninjaapp_quest_page VALUES (25, 'Donuts', false, 'Donuts', 'De duurzame (gezonde) zone, voor zowel de wereld als onszelf, is de middelste zone. De mensheid begeeft zich veel in de binnenste of buitenste zones. 

Bedenk op welke onderdelen van je leven jij je in de binnenste of buitenste zone bevindt. Schrijf ze hieronder op of teken je eigen donut en plaats daar een foto van.', 'default_images/default_quest.png', 14, false, 'Wat vind je ervan? Wil je iets veranderen?', 3, false);
INSERT INTO public.ninjaapp_quest_page VALUES (27, 'Nulmeting', true, '', 'Wat in deze cirkel is voor jou het belangrijkst? Geef in de witte cirkels de volgorde van 1 tot 8 aan.', 'images/quests/Nulmeting.png', 12, false, 'Teken deze cirkel na of bewerk hem in een foto-app.', 1, false);
INSERT INTO public.ninjaapp_quest_page VALUES (28, 'Nulmeting', false, '', 'Geef voor elk van de onderwerpen in de cirkel aan hoe je je er over voelt en trek een lijn van cijfer tot cijfer.  (1 = zeer gestressed, 5 = matig relaxed, 10 = zeer relaxed)', 'default_images/default_quest.png', 12, false, 'Zie je waar je het minst relaxed bent?', 2, false);
INSERT INTO public.ninjaapp_quest_page VALUES (30, 'Nulmeting', false, '', 'Bedenk nu voor ieder onderwerp welk cijfer je in een gewenste situatie zou geven. Dit mogen natuurlijk allemaal 10-en zijn, maar dat hoeft niet.

Trek nu per onderwerp een tweede lijn in een andere kleur van doelcijfer tot doelcijfer. 

Bekijk waar je het grootste verschil ziet en waar je in de eerste stap de hoogste prioriteit aan hebt gegeven. Waar wil je veranderingen gaan maken?', 'default_images/default_quest.png', 12, false, 'Plaats een foto van jouw resultaat en/of beschrijf jouw inzichten.', 3, false);
INSERT INTO public.ninjaapp_quest_page VALUES (35, 'In jouw ogen', false, '', 'Prik een moment om dit ''In your eyes-spel'' met diegene te spelen. 

Dus: zet een timer en kijk iemand vijf minuten lang in de ogen aan zonder iets te zeggen.

Schrijf daarna op wat jij ervaart. Durf je de ander ook te vragen wat hij of zij voelde? Schrijf ook dat op.', 'default_images/default_quest.png', 15, false, 'Beschrijf jouw of jullie ervaring.', 3, false);
INSERT INTO public.ninjaapp_quest_page VALUES (34, 'In jouw ogen', false, '', 'Als je iemand eens vijf minuten in de ogen mag kijken, zonder iets te zeggen, wie kies je dan? 

Ga je voor comfortabel en kies je een beste vriend? Ga je voor neutraal en kies je iemand die je nog niet goed kent? Of ga je de confrontatie aan met iemand waar je een spanning bij voelt?', 'default_images/default_quest.png', 15, false, 'Wie kies je?', 2, false);
INSERT INTO public.ninjaapp_quest_page VALUES (37, 'Vertragen', false, '', 'Loop nu een aantal minuten door op halve snelheid. Merk op hoe de vloer of het gras onder je voeten voelt. Ga even stil staan. Wiebel met je tenen. Wat voel je?', 'default_images/default_quest.png', 16, false, 'Merk op hoe het voelt om op halve snelheid te lopen.', 2, false);
INSERT INTO public.ninjaapp_quest_page VALUES (40, 'De teken-check', false, '', 'Weet je nog dat je vroeger buiten in de bossen speelde? En dat je daarna altijd samen met je vader of moeder een ''teken-check'' moest doen? Je had er nooit veel zin in, maar het moest gebeuren. Een onopgemerkte beet van een teek zou problemen op kunnen leveren. Gelukkig was ie aan zijn zwarte uiterlijk makkelijk te herkennen. 

Je kunt je voorstellen dat zo''n zelfde check op spanning in je lichaam ook best handig kan zijn.', 'default_images/default_quest.png', 17, false, 'Voer jij wel eens een ''spanningscheck'' uit?', 1, false);
INSERT INTO public.ninjaapp_quest_page VALUES (41, 'De teken-check', false, '', 'https://www.youtube.com/watch?v=0DI6EIdCY5o', 'default_images/default_quest.png', 17, false, 'Probeer mee te doen met de meditatie uit deze video. Wat komt er in je op?', 2, true);
INSERT INTO public.ninjaapp_quest_page VALUES (42, 'De teken-check', false, '', 'Net als een teek kan langdurig onopgemerkte spanning in je lichaam ook voor moeilijkheden zorgen. Het kan daarom handig zijn om die goede oude teken-check weer eens in te voeren, maar dan om spanning in je lichaam te herkennen. 

Ga de komende week iedere dag even je hele lichaam na. Je kunt daarbij de video als voorbeeld gebruiken.', 'default_images/default_quest.png', 17, false, 'Hoe voelt het aan? Voel je ergens spanning? Of pijn?', 3, false);
INSERT INTO public.ninjaapp_quest_page VALUES (44, 'Het spelende kind', false, '', 'Klop aan bij je vader, moeder, opa, oma of iemand anders die jou als kind heeft meegemaakt. Wat hebben zij te vertellen over hoe jij als kind graag speelde?', 'default_images/default_quest.png', 18, false, 'Wat kwam er ter sprake?', 2, false);
INSERT INTO public.ninjaapp_quest_page VALUES (31, 'Nulmeting', true, 'Waarom deze quest?', 'Stress werkt vaak door op verschillende gebieden in je leven. Wanneer je het gevoel hebt dat je op meerdere vlakken achter de feiten aan loopt kan dat de druk verhogen. 

De quest ''Nulmeting'' kun je gebruiken om <b>in kaart te brengen waar je op dit moment staat</b>. Welke elementen in jouw leven zijn volgens jou nu niet op orde? Waarom? Is dat erg? En waar wil je verandering in brengen? 

Je kunt jouw Levenswiel <b>bespreken </b>met jouw Sensei in de game of iemand uit je persoonlijk netwerk. Deel aan welk element uit de cirkel jij als eerste aandacht wilt gaan besteden en hoe je dit wilt gaan doen. Wat heb je daarvoor nodig? Is er iemand die je om hulp kan/wilt vragen?', 'images/quests/Wielen_Loesje.jpg', 12, true, '-', 0, false);
INSERT INTO public.ninjaapp_quest_page VALUES (33, 'In jouw ogen', false, '', '"The eye, the window of the soul, is the chief means whereby the understanding can most fully and abundantly appreciate the infinite works of nature; and the ear is second." 

- <b> Leonardo da Vinci </b>', 'default_images/default_quest.png', 15, false, 'Wanneer heb jij voor het laatst 
iemand in zijn of haar ogen gekeken?', 1, false);
INSERT INTO public.ninjaapp_quest_page VALUES (36, 'Vertragen', false, '', 'Voel jij wat er in je lijf gebeurt? Om daar achter te komen kun je vertragen.

Doe je schoenen uit en ook je sokken. Begin eens door je  kamer te lopen, of door je tuin wanneer je die hebt. En dan gewoon op het tempo dat je altijd loopt. Na 30 seconden halveer je jouw tempo. Loop nu op de helft van je normale snelheid. En neem eens waar of je jouw lijf kunt voelen. En merk eens op wat voor gedachten je krijgt.', 'default_images/default_quest.png', 16, false, 'Wat merk je op wanneer jij vertraagd?', 1, false);
INSERT INTO public.ninjaapp_quest_page VALUES (38, 'Vertragen', false, '', 'De kracht van vertragen ligt in de herhaling.

Ga aankomende week 3x per week (vaker mag!) met je blote voeten maximaal 5 minuten aan de wandel door je huis of door je tuin. Halveer iedere keer na 30 seconden je snelheid. Sta in de vertraging ook af en toe stil en wiebel met je tenen.

Merk op wat er verandert na 1 week. Wanneer je het fijn vindt, blijf je dit gewoon doen na deze week.', 'default_images/default_quest.png', 16, false, 'Wat merk jij op wanneer je dit ‘met blote voeten lopen’ herhaalt?', 3, false);
INSERT INTO public.ninjaapp_quest_page VALUES (43, 'Het spelende kind', false, '', 'Trek je voor deze quests even terug op een plek waar je ongstoord alleen kunt zijn. Ga in een comfortabele houding zitten of liggen. Check of je billen of voeten de grond raken. Zo niet, via welke weg ben jij verbonden met de aarde? Wees je daarvan bewust. 

Sluit dan je ogen en <b>probeer je gedachten terug te brengen naar je kindertijd</b>. Waar speelde jij het liefst mee? Welke spellen vond je heel erg leuk en welke niet? Welk gevoel krijg je daarbij?', 'default_images/default_quest.png', 18, false, 'Wat komt er in je op? Schrijf zoveel mogelijk op.', 1, false);
INSERT INTO public.ninjaapp_quest_page VALUES (45, 'Het spelende kind', true, '', 'In 1996 introduceerde gamewetenschapper Richard Bartle een theorie die zegt dat spelers op te delen zijn in 4 speltypen.', 'images/quests/PlayerTypes.png', 18, false, 'Herken jij jezelf in één of meerdere van deze type spelers?', 3, false);
INSERT INTO public.ninjaapp_quest_page VALUES (46, 'Het spelende kind', false, 'Waarom deze quest?', 'Als je meer wilt weten over wat voor effect spelen op ons psychologisch welzijn kan hebben, dan kan je dit gratis toegankelijke artikel van Rutger Bregman lezen:

<link="https://bit.ly/2uqUKwy"><color=#285E38><u>

Waarom onze kinderen steeds minder spelen en wij met een burn-out thuis zitten. via de Correspondent </color></u></link>

Vond je het prettig om te mediteren en aan jouw spelende kind te denken? Bekijk dan ook deze video en ga mee op reis naar je ''innerlijke kind''. Het kan helpen om er een foto van jezelf als klein kind bij te pakken.

Video afspelen vanaf 0.30minuten: 

<link="https://www.youtube.com/watch?v=8G6eUdhjvVI"><color=#285E38><u>

Meditatie Innerlijk kind via YouTube</color></u></link>

Tenslotte nog een gedicht van Willem Wilmink, hij schreef een gedicht over de tijd waarin we speelde op straat:

<link="https://dewerelddraaitdoor.bnnvara.nl/media/381193"><color=#285E38><u>

Gedicht ''Auto''s'' via DWDD</color></u></link>', 'default_images/default_quest.png', 18, true, 'default question', 0, true);
INSERT INTO public.ninjaapp_quest_page VALUES (48, 'De teken-check', false, 'Waarom deze quest?', '<b> Heb jij het altijd direct door als er spanning in je lichaam huist? </b>

Net als een teek die zich nestelt in je huid, merk je spanning in je lichaam soms niet direct op. Langdurig aanhoudende spanning kan echter, net als een te lang zittende teek, voor vervelende klachten zorgen.

<b>Tegelijkertijd kan het je een hoop vertellen! </b> Voelen waar en wanneer er spanning in je lichaam ontstaat, kan je helpen om richting te geven aan de keuzes die je maakt in je leven. 

Het voelen van die spanning is zoals gezegd soms lastig, maar dat kun je trainen. Je zult merken dat door structureel een <b> ‘spanningscheck’ </b> te doen, dit steeds makkelijker gaat. 

<i> De check op spanning in je lichaam is eigenlijk een soort medisch onderzoek dat je zelf uitvoert. Jij kan tenslotte het beste voelen wat je voelt. </i>

Binnen Mindfulness wordt deze oefening vaak <b>''Bodyscan'' </b>genoemd. "Het doel van de bodyscan is oefenen met mindfulness oftewel opmerkzaamheid, in dit geval aandacht hebben voor wat er in je lichaam gebeurt. In het dagelijks leven ben je vooral bezig met denken en doen. Deze oefening brengt je juist in contact met je lijf," zegt  <link="gezondheidsnet.nl"><color=#075e36><u>GezondheidsNet</u></color></link>. 

Bij het doen van de oefening kun je het geluid van de video in deze quest als begeleiding gebruiken. 

<i> Je kunt ook onderstaande stappen een paar keer doorlezen en de oefening zonder begeleiding proberen uit te voeren. </i>

<b> Dwalen je gedachten gedurende de oefening af? </b> Geen paniek. Zeker als je net begint met oefenen zal dit vaak voorkomen. Dat geeft niets, zolang je je aandacht telkens weer rustig terugbrengt naar het lichaamsdeel dat je op dat moment ‘aan het checken’ was. 

Het kan zijn dat je jezelf tijdens de spanningscheck onrustig gaat voelen. Misschien voelt het wel ongemakkelijk. Vermoeidheid kan ook zomaar ineens gevoeld worden. Ook kunnen irritaties ontstaan, of kan er weerstand tegen dit ''stil taan'' opkomen. Dat is niet erg of gek. 

Het zijn allemaal signalen om vooral door te gaan met het stil staan en met School for Ninja. 

<b>Stappen bodyscan </b> (Bron: <link="gezondheidsnet.nl"><color=#075e36><u>GezondheidsNet</u></color></link>)

✭ Ga comfortabel liggen op een rustige plek, op een matje of op bed. Leg eventueel een kussentje onder je hoofd en knieën. Voelt liggen onprettig, dan kun je de bodyscan ook zittend doen. Leg je armen langs je lichaam, strek je benen uit en laat je voeten iets naar buiten vallen. Doe je ogen dicht.

✭ Let op je ademhaling. Je hoeft niet dieper of anders adem te gaan halen, merk gewoon op hoe je ademt. Voel hoe bij elke inademing de lucht naar binnen stroomt, je borstkas vult en je buik laat opbollen. En hoe bij elke uitademing de lucht weer naar buiten gaat door je neus of mond.

✭ Wees je bewust van de manier waarop je ligt. Je hoeft geen enkele spier aan te spannen als je ligt, de aarde draagt je wel. Merk op of je toch ergens spanning voelt. Of pijn. Voel je waar je lichaam de mat raakt? Op welke plekken?

✭ Wat is je gemoedstoestand? Ben je vrolijk, verdrietig, chagrijnig, gestrest, ontspannen, moe? Signaleer het alleen maar, je hoeft er niets aan te veranderen.

✭ Daarna ga je stap voor stap je hele lichaam ''scannen’. Bekijk eerst eens wat je voelt in je linkertenen. Je hoeft ze niet te bewegen: ervaar alleen maar wat je voelt. Warmte, kou, wind die langs je huid strijkt, spanning, pijn, de stof van je sok? Misschien voel je niets, dat is ook prima.

✭ Vervolgens ga je verder met je kuit, knie, bovenbeen. Eerst links, dan rechts. Vervolgens komt de rest van je lijf aan bod, net zo lang tot je bij je kruintje bent.

✭ Voel nog even na wat de bodyscan voor effect gehad heeft.

✭ Als je er klaar voor bent, beweeg je eerst je tenen en vingers. Rol eventueel op je zij. Doe je ogen weer open en beëindig de bodyscan.

Bron afbeelding:  <link="gezondheidsnet.nl"><u><color=#075e36>GezondheidsNet</color></u></link>', 'images/quests/Schermafbeelding_2019-02-19_om_16.18.44.png', 17, true, 'default question', 0, false);
INSERT INTO public.ninjaapp_quest_page VALUES (22, 'Stoppen of doorrijden?', false, 'Waarom deze quest?', '<b>Iedere dag heb je te maken met regels. </b>

Allereerst de regels van de <b>maatschappij</b>, die structuur in de samenleving aanbrengen. Het stoplicht is daar ook een voorbeeld van. Het zijn regels waarover we hebben afgesproken ons eraan te houden om chaos en gevaarlijke situaties te voorkomen.

<b>Organisaties </b>waar je je bij aansluit, zoals een opleidingsinstituut, bedrijf of sportclub kennen weer hun eigen regels. 

Maar naast deze geschreven regels, die nodig zijn om chaos en gevaarlijke situaties te voorkomen, zijn er ook tal van ongeschreven <b> sociale gedragsregels</b>. Deze zijn vaak (bedrijfs-) cultuurgebonden. 

Zo kan het zijn dat jij een boer laten in gezelschap niet netjes vindt, terwijl dat in andere culturen als compliment wordt beschouwd. Volgens de ''norm'' in de maatschappij kan het ook zijn dat er van je verwacht wordt dat je voor een hoogst haalbare opleiding gaat en een bepaalt carrièrepad volgt. Dat je er smashing (volgens het schoonheidsideaal) uit ziet, dat je éérst trouwt voordat je kinderen krijgt, en ga zo maar door. 

Ongemerkt (of bewust) leg je <b>jezelf </b< ook nog veel regels op, die vaak voortkomen uit eerdere ervaringen en de manier waarop jouw omgeving jou beïnvloedt. De regel om 5 keer per week in de sportschool te staan bijvoorbeeld, alleen maar hoge cijfers te halen, geen ruzie te maken in het openbaar, niet te huilen op je werk, mensen niet langer dan 3 seconden in hun ogen aan te kijken, dat veel geld hebben belangrijk is, of dat op zaterdag en zondag het weekend plaatsvindt. 

<b> De ''purpose'' van School for Ninja is spelen met regels. </b>  Met spelen bedoelen we in dit geval het vóórdat je gedachteloos een bepaalde regel volgt, bewust stilstaan bij deze regel en beoordelen of deze regel eigenlijk wel klopt, of past bij de situatie. Zo niet, dan zou je ''m kunnen aanpassen. 

In deze quest gebruiken we het voorbeeld van een herkenbare regel zoals het stoplicht, ter vergelijking met een ongeschreven regel die jij volgt. Het antwoord op de vraag of je bij een rood stoplicht zou stoppen of doorrijden wanneer er geen verkeer te zien is, zegt iets over of jij een regel volgt of niet, ongeacht de situatie. Ben je je bewust van het effect van die keuze? Stoppen betekent dat je moet wachten voor ''niets'', doorrijden brengt wel een risico met zich mee. 

We proberen je met deze quest natuurlijk niet te stimuleren om verkeersregels te breken. 

Wel stimuleren we je om stil te staan bij regels die je in je leven volgt, die misschien nooit, of in sommige situaties geen, positief resultaat opleveren.  

<b> Samen maken we de regels  </b>

''Samen'' in de term <b>samenleving</b> is belangrijk omdat we samen het systeem zijn. Samen bepalen we de geschreven en ongeschreven regels. Samen zijn we sterk. Dat is ook de reden dat de School for Ninja een community is. Samen hebben we de macht om systemen te veranderen. Hoe we het systeem willen veranderen, moet samen bepaald worden. 

Misschien geloof je niet dat we met een online platform als dit echte verandering teweeg kunnen brengen op bijvoorbeeld jouw werkafdeling. Het kan inderdaad zijn dat het niet direct gebeurt, maar wat je aandacht geeft groeit. Als we met vele onze aandacht stoppen in systemen die we nu wél waarderen, dan worden die systemen groter en andere uiteindelijk kleiner. Kamehameha!

Er bestaat wetenschap over hoe een community ideeën het beste kan verspreiden. Dat heeft te maken met verbindingen binnen netwerken en verbindingen tussen netwerken. 

In plaats van de wetenschap hier aan je uit te leggen, raden we je aan een mini-game te spelen uit een ander netwerk over netwerken. De game is in het engels en heeft een speeltijd van ongeveer 30 minuten. Als je Engels niet heel goed is, kun je de game het beste samen met iemand spelen die de Engelse taal goed begrijpt.

Je vindt de game hier: 

<link="https://ncase.me/crowds/"><color=#285E38><u>

Game ''The wisdom and/or madness of crowds'' door Nicky Case</color></u></link>', 'default_images/default_quest.png', 13, true, 'default question', 4, false);
INSERT INTO public.ninjaapp_quest_page VALUES (39, 'Vertragen', true, 'Waarom deze quest?', '"Als woelig water tot rust komt, wordt het langzaam helder." -Leo Tse

Toen de trein werd uitgevonden waren er protesten. De hoge snelheid zou niet goed voor ons zijn. Sindsdien zijn we steeds sneller gaan leven. Ooit zijn we horloges gaan dragen om de trein te halen. Nu dragen we horloges en mobiele telefoons om inkomende berichten op te checken, onze stappen te tellen en nog veel meer. We krijgen steeds meer prikkels binnen op een dag. Prikkels zijn een aanleiding voor gedachten. 

Er is iemand die heeft het aantal gedachten dat we hebben gemeten en kwam uit op zo’n 50.000 gedachten per dag. Dat zijn er nogal wat. En ze kosten allemaal energie. Ons dagelijks tempo ligt zo hoog, dat we niet meer waarnemen wat er in ons lijf gebeurt. 

Soms krijgen we een onaangenaam gevoel, stress, of zelfs een paniekaanval zonder dat je snapt waardoor dat komt. Het zou heel goed kunnen dat het door een onbewuste ervaring komt. De meeste van die gedachten komen onderbewust langs. Vaak hebben we niet eens in de gaten dat we aan het denken zijn en geven we dus energie uit zonder het in de gaten te hebben. Een binnenkomende prikkel die je verhalende zelf niet registreert, maar je lichaam wel degelijk ondergaat. Er zijn op een dag ontzettend veel prikkels, beelden of geluiden, die je onbewust wegfiltert. 

Door te vertragen ben je weer in staat om dat wel waar te nemen. 

Wanneer je dat voor de eerste keer doet kan het zijn dat je jezelf tijdens de vertraging onrustig gaat voelen. Misschien voelt het wel ongemakkelijk. Vermoeidheid kan ook zomaar ineens gevoeld worden. 

Ook kunnen er tijdens de het vertragen irritaties ontstaan, of kan er weerstand tegen de vertraging opkomen.

Dat zijn allemaal signalen om vooral door te gaan met vertragen en met deze Ninja-training.

*Als je hooggevoelig bent komen prikkels nog harder binnen. Hooggevoelige mensen ervaren een verandering van licht bewuster en kunnen slechter tegen lawaai. Een op de vijf mensen is hooggevoelig. In deze School for Ninja is dat aantal waarschijnlijk nog hoger, omdat hooggevoelige mensen eerder stress ervaren. Tevens ervaren mensen met heftige stress prikkels intenser, omdat hun prikkel-limiet is bereikt.', 'images/quests/Prikkels.jpg', 16, true, 'default question', 0, false);
INSERT INTO public.ninjaapp_quest_page VALUES (49, 'Ninja Nu', false, '', 'Ninja ervaren heel goed wat er om hen heen gebeurt. Ze leven in het ''hier en nu''. 

Jij hebt NU een Ninja alter-ego. Voor deze quest ga je jouw dagelijks leven deze week benaderen vanuit dit Ninja alter-ego. Je laat dus even los wat niet hier of nu is en geeft aandacht aan wat er wel is.

Verlangens die hebt, naar dingen of situaties die je graag zou willen maar er nu nog niet zijn, probeer je los te laten.', 'default_images/default_quest.png', 19, false, 'Welk verlangen laat je deze week even los?', 1, false);
INSERT INTO public.ninjaapp_quest_page VALUES (50, 'Ninja Nu', false, '', 'Leg thuis een zo groot mogelijk blanco vel neer en pak alle kranten, tijdschriften, pennen, stiften, krijt, verf etc. die je hebt.

Maak halverwege de week een ''collage'' van woorden en/of beelden die jij vindt passen bij de situatie zoals jouw Ninja alter-ego die nu ervaart. Wat heb je deze week gevoeld, meegemaakt, gezien of gedacht? 

Deze ''collage'' kun je aan jouw Story toevoegen door er een foto van te maken.', 'default_images/default_quest.png', 19, false, 'Wat valt je op in jouw resultaat?', 2, false);
INSERT INTO public.ninjaapp_quest_page VALUES (51, 'Ninja Nu', false, '', 'Pak de collage er aan het eind van de week nog eens bij. Omcirkel de drie woorden of beelden die je het meeste raken. 

Kijk naar de drie omcirkelde beelden of woorden. Stel dat het verlangen dat je op pauze hebt gezet er over 1 jaar nog niet is, maar deze drie omcirkelde beelden of woorden wel.', 'default_images/default_quest.png', 19, false, 'Hoe zou dat zijn voor jou?', 3, false);
INSERT INTO public.ninjaapp_quest_page VALUES (79, 'Chloe talks', false, '', 'De video van Chloe is fictie. Zover zijn we nog niet. Echter is dit geen onwaarschijnlijk toekomstscenario. Robots gaan een aantal van onze banen overnemen. <b>Kijk eens naar jouw collega''s</b>. Welke skills zou Chloe van hen niét kunnen overnemen?', 'default_images/default_quest.png', 26, false, 'Vertel!', 2, false);
INSERT INTO public.ninjaapp_quest_page VALUES (69, 'Wie niet waagt...', false, '', '<b>Een comfortabel leven is geen garantie voor een stress-vrij leven</b>. De bore-out is hier een bewijs van. Net als te veel uitdaging, kan géén uitdaging of het ervaren van een sleur, leiden tot klachten zoals vermoeidheid en neerslachtigheid.', 'default_images/default_quest.png', 24, false, 'Ervaar jij een sleur of juist teveel hectiek?', 1, false);
INSERT INTO public.ninjaapp_quest_page VALUES (72, 'Wie niet waagt...', false, '', 'Waarschijnlijk zit je soms in de ene zone en soms in de andere. Het prettigst is een <b>goede balans tussen de groei zone, met af en toe een beetje comfort</b>. Wat zou jij in je dagelijks leven kunnen aanpassen om dichter bij die balans te komen?', 'default_images/default_quest.png', 24, false, 'Vertel!', 3, false);
INSERT INTO public.ninjaapp_quest_page VALUES (75, 'Ted talks', false, '', 'Kijk deze week elke dag een Ted talk (www.Ted.com ideas worth spreading). Kies verschillende topics. <b>Kies experimenteel</b>: talks die je interesseren, maar waar je nog niet heel veel vanaf weet.', 'default_images/default_quest.png', 25, false, 'Over welke onderwerpen heb je de afgelopen week geleerd?', 2, false);
INSERT INTO public.ninjaapp_quest_page VALUES (80, 'Chloe talks', false, '', 'Stel, er zijn overal ''Chloe''s... hoe zou jij je dan onderscheiden? <b>Welke menselijke skill-set kun jij daarvoor verder ontwikkelen?</b> Kun je een cursus vinden over deze skill die je (via je werkgever) kunt uitvoeren?', 'default_images/default_quest.png', 26, false, 'Welke cursus lijkt je interessant?', 3, false);
INSERT INTO public.ninjaapp_quest_page VALUES (82, 'JOMO', false, '', 'Veel mensen hebben last van het bekende FOMO, de Fear of Missing Out. Oftewel: de angst om iets belangrijks, leuks of unieks te missen. FOMO is van alle tijden, maar het probleem wordt steeds groter door de ontelbare mogelijkheden die we iedere dag weer hebben. Een actief leven is gezond maar wanneer je al je dagen volstopt en daarmee <b>geen ruimte meer hebt voor rust en reflectie, kunnen zelfs ''ontspannen'' activiteiten, zoals afspreken met vrienden, stress opleveren</b>.', 'default_images/default_quest.png', 27, false, 'Heb jij wel eens last van FOMO?', 1, false);
INSERT INTO public.ninjaapp_quest_page VALUES (83, 'JOMO', false, '', 'Ninja weten goed waar ze aandacht aan willen besteden en waar aan niet. Door sommige dingen bewust niet te doen, maken ze ruimte om even stil te staan. Stil bij het leven, bij het hier en nu dat altijd in verandering is. <b>Ze ervaren de JOMO (Joy of Missing Out)</b>, het tegenovergestelde FOMO. Deze week ga jij JOMO ervaren door OF één week geen social media te gebruiken (dus ook geen WhatsApp) OF een paar dagen in deze week, een uur per dag <b>niets</b> doen.', 'default_images/default_quest.png', 27, false, 'Hoe ga jij deze week JOMO trainen?', 2, false);
INSERT INTO public.ninjaapp_quest_page VALUES (84, 'JOMO', false, '', 'Dankzij het creëren van rust en ruimte kunnen Ninja het hier en nu beter ervaren. Ze zien wat er gebeurt, hoe de wereld in beweging is. Ninja remmen af, creëren ruimte en bewegen dan mee. In Theory-U heet dit presencing: <b>present-sencing</b>, op de infopagina kun je hier meer over lezen.', 'default_images/default_quest.png', 27, false, 'Hoe heb jij jouw weekje JOMO ervaren?', 3, false);
INSERT INTO public.ninjaapp_quest_page VALUES (11, 'Hoe gaat het?', false, 'Hoe gaat het?', 'Stel jezelf nu in een paar zinnen voor. Wie ben jij en hoe gaat het met je? 

Je hebt nu een Ninja alter-ego, dus probeer je ware identiteit niet bloot te geven.', 'default_images/default_quest.png', 10, false, 'Vertel!', 1, false);
INSERT INTO public.ninjaapp_quest_page VALUES (14, 'Hoe gaat het?', true, 'Waarom deze quest?', 'De vraag ''Hoe gaat het'' beantwoorden we in sociale situaties meestal vanuit de ''verhalende versie'' van ons ''zelf''. Dit wil zeggen dat wanneer we de vraag ''Hoe gaat het'' beantwoorden, we iets benoemen uit de geschiedenis of de toekomst wat uitdrukt hoe het met ons gaat.

<b>De verhalende zelf</b>

De verhalende zelf is de ''zelf'' die wordt aangesproken wanneer je vraagt ''Hoe was je trip naar Barcelona?''. Wanneer je de vraag ''Hoe gaat het'' vanuit de verhalende zelf beantwoordt, leg je de nadruk op hoe gelukkig en tevreden je bent als je denkt aan je leven. Je benadert je leven in die situatie als een reeks van ervaringen. 

Naast de ''verhalende zelf'' bestaat er een ''ervarende zelf''. Wanneer je vanuit deze ''zelf'' antwoordt op hoe het met je gaat, luister je naar je onderbuikgevoel. 

<b>De ervarende zelf</b>

De ervarende zelf leeft in de tegenwoordige tijd. Hij kan het verleden ophalen maar heeft in principe alleen het ''nu''. Het is deze ''zelf'' die een dokter aanspreekt wanneer hij of zij vraagt of iets pijn doet. Als je de vraag ''Hoe gaat het'' vanuit de ervarende zelf beantwoordt, leg je de nadruk op hoe je je voelt.  

De verwarring tussen de ervarende zelf en de verhalende zelf bepaalt voor een groot deel de ontoereikendheid van ons geluk. In de bepaling of het ''goed met ons gaat'' hechten we over het algemeen veel waarde aan onze herinneringen, ons verhaal, en minder aan de ervaring van het nu. 

Vaak gebruiken we de verhalende zelf omdat anderen zich daar in kunnen verplaatsen. Een uitleg als ‘Goed, ik ben net gaan samenwonen en heb een nieuwe baan’ past in het systeem (carrière maken, een relatie hebben) en stelt de mensen om je heen gerust. 

<i>Ondertussen kun je iets anders voelen. </i>

Zo kan je verhalende zelf een positief beeld geven, maar je ervarende zelf misschien niet. 

Andersom werkt het natuurlijk ook zo: je verhalende zelf kan een negatief beeld schetsen, terwijl je ervarende zelf zich goed voelt. 

Nobelprijswinnaar Daniel Kahneman vertelt meer over ''experience'' versus ''memory'' in deze <link="https://bit.ly/2C773B5"><color=#075e36>Ted-talk.</color></link>

Bron afbeelding: Mark Mayers', 'images/quests/labyrinth_mark_mayers.jpg', 10, true, '-', 0, false);
INSERT INTO public.ninjaapp_quest_page VALUES (18, 'Jouw Ninja Naam', true, 'Waarom deze quest?', '<i>“Man is least himself when he talks in his own person. Give him a mask, and he will tell the truth.”</i> <b>- Oskar Wilde</b>

Je hebt waarschijnlijk al een hoop profielen: LinkedIn, Facebook, Instagram… Maar ook fysieke profielen: op het werk, bij je familie, thuis. Elke omgeving vraagt weer om een andere beschrijving, een ander gedrag. Elke wereld bestaat uit allerlei regels die jouw zichtbare persoonlijkheid en gedrag vormen. 

De School for Ninja is nog een alternatieve wereld. Ook hier vragen we je een profiel aan te maken. In de School for Ninja heb je straks een avatar, een beschrijving, krachten en naarmate je verder in de wereld komt een verhaal. Echter, staat dit profiel los van al je andere profielen. Nergens koppelen we je account zichtbaar aan je ‘echte wereld’ naam. Tenzij je zelf in je profiel of ervaringen verwijst naar een van die andere werelden, ben je hier anoniem. 

Deze eerste quest is een belangrijke stap in het ‘Ninja worden’.

Wat een Ninja is? Ten eerste is een ninja een mens. Bij het mens zijn horen allerlei eigenschappen, zoals liefhebben en gestrest zijn. Ninja accepteren ‘het mens zijn’. Ten tweede leeft de Ninja bewust. Ninja snappen hoe hun lichaam en geest en de omgeving samenwerken en maken met die kennis bewuste keuzes. 

Deze alternatieve, anonieme identiteit van de Ninja geeft je de kans om bestaande regels, patronen en verwachtingen los te laten, en er van een afstandje naar te kijken. We hopen dat je zo dichter bij je authentieke zelf kan komen.

In de psychology wordt regelmatig het ACT-hexaflex (Acceptence and Commitment Therapy) gebruikt, zie de afbeelding bovenaan deze pagina. Het ACT-hexaflex illustreert mooi hoe alle ACT-processen met elkaar in verband staan en samen psychologische flexibiliteit vormen

Betekenis van deze zuilen in het kort:

- Acceptatie: Het actief uitnodigen van vervelende gedachten, gevoelens en omstandigheden.

<b>- Defusie: Loskomen van je gedachten, zodat deze je minder snel zullen raken.</b>

- Zelf als Context: Een andere, meer flexibele relatie met jezelf creëren.

- Hier en Nu: In contact komen met het hier en nu.

- Waarden: Ontdekken wat je werkelijk belangrijk vindt in het leven.

- Toegewijd Handelen: Dingen gaan ondernemen op basis van je waarden.

Jouw Ninja avatar zal je helpen met onder andere <b>Defusie</b>. Ook de andere zuilen komen terug in deze School for Ninja.', 'images/quests/The-ACT-hexaflex.png', 11, true, '-', 0, false);
INSERT INTO public.ninjaapp_quest_page VALUES (32, 'Donuts', false, '', 'Volgens psychiater Dr. Witte Hoogendijk is stress het gevoel dat je hebt als je je moet aanpassen aan iets wat je aanpassingsvermogen te boven dreigt te gaan. 

De aarde ervaart nu ook op een hoop vlakken ''stress''. De mensheid verbruikt nu namelijk meer grondstoffen dan de wereld aankan. Als we deze leefstijl door willen zetten hebben we twee planeten nodig en dat gaat het aanpassingsvermogen van de aarde te boven.  

''De Donut Economie'' 

Een groep van wetenschappers onderzocht dat er 9 kritieke grenzen zijn aan wat onze aarde kan hebben. Als we die grenzen voorbij gaan kunnen we de aarde ‘uitputten’ en het voortbestaan ervan in groot gevaar brengen. Econoom Kate Raworth visualiseerde deze theorie als een donut. In deze donut geeft ze aan dat de buitenzones van de negen gebieden gevaarlijk zijn voor het voortbestaan van de aarde en dus de mensheid, maar dat er ook niet-duurzame binnenzones zijn. Als we bijvoorbeeld te weinig eten produceren is er sprake van armoede / honger.  

''De Leerzone Donut'' 

Voor ons persoonlijke stressniveau geldt hetzelfde. Er zijn grenzen aan beide kanten. Als je teveel stress ervaart kun je bijvoorbeeld in paniek raken, als je te weinig stress ervaart kan dat tot verveling leiden. 

De kunst is om zoveel mogelijk in de gele ''stretch zone'' te bewegen. Door bewust te worden van op welke vlakken je in je leven al in deze stretch zone zit of niet, kun je duurzamere keuzes gaan maken. Heb je bijvoorbeeld het idee dat je verveeld bent op je werk? En vind je dit erg? Dan kun je daar een nieuwe richting aan gaan proberen te geven. Heb je het idee dat de combinatie van je werk en je sociale activiteiten zich in de panic zone bevindt? Misschien is het dan tijd om ergens een stapje terug te nemen.', 'default_images/default_quest.png', 14, true, '-', 0, false);
INSERT INTO public.ninjaapp_quest_page VALUES (16, 'Jouw Ninja Naam', false, 'Jouw Ninja Naam', 'Stap 2: Beantwoord deze vraag: <b> Als je stress hebt, waar voel je dat dan het meest? </b>

1: Ademhaling, 2: Buik, 3: Hoofd, 4: Spieren, 5: Geen van allen

<i>Voor jouw antwoord staat een nummer. </i>
1 Welk <b>dier</b> vind je leuk?
2 Welk <b>weerelement</b> past bij jou?
3 Wat is je favoriete <b>plant of bloem</b>?
4 Welke <b>hobby</b> vind je leuk? 
5 Welk <b>eten</b> vind je leuk?', 'default_images/default_quest.png', 11, false, 'Beantwoord hieronder de vraag
achter jouw nummer:', 2, false);
INSERT INTO public.ninjaapp_quest_page VALUES (71, 'Wie niet waagt...', true, '', 'Bekijk deze afbeelding', 'images/quests/Schermafbeelding_2019-04-15_om_17.17.48.jpg', 24, false, 'In welke zone bevind jij je nu?', 2, false);
INSERT INTO public.ninjaapp_quest_page VALUES (74, 'Ted talks', true, '', 'De geest uit Alladin vervult wensen. Wat zou jij wensen? Kan jij deze wens zelf in vervulling brengen?', 'images/quests/Schermafbeelding_2019-04-02_om_13.02.20.jpg', 25, false, 'Zou jij de wereld willen veranderen? Hoe?', 1, false);
INSERT INTO public.ninjaapp_quest_page VALUES (78, 'Chloe talks', false, '', 'https://youtu.be/WT5K3aUagm4', 'default_images/default_quest.png', 26, false, 'Bekijk deze video. Denk jij dat mensen een ziel hebben?', 1, true);
INSERT INTO public.ninjaapp_quest_page VALUES (76, 'Ted talks', false, '', 'Welk onderwerp fascineert je het meest? Misschien kun je je in jouw werkzaamheden bezighouden met dit onderwerp.', 'default_images/default_quest.png', 25, false, 'Hoe kun je dit onderwerp toevoegen aan jouw werkzaamheden?', 3, false);
INSERT INTO public.ninjaapp_quest_page VALUES (47, 'In jouw ogen', true, 'Waarom deze quest?', '<i>"De ogen zijn de spiegel van de ziel" </i> - <b>Da Vinci</b>

Naast onze geliefde, kijken we meestal niet veel mensen diep in de ogen. Toch kan het een hoop teweeg brengen. 

<b>Begrip </b>

Als twee personen geboeid met elkaar praten, worden hun pupillen ongeveer even groot. Dit is een teken dat ze belangstelling voor elkaar hebben. Bij die zogenaamde pupilsynchronisatie spelen spiegelneuronen vermoedelijk een rol. Deze zenuwcellen zorgen ervoor dat we de ander imiteren, beter begrijpen en ons beter in hem kunnen inleven. Japanse onderzoekers zijn nieuwsgierig naar de rol die oxytocine speelt bij pupilsynchronisatie. Deze hormoonachtige stof blijkt namelijk vrij te komen wanneer we elkaar diep aanstaren. De onderzoekers ontdekten zelfs verhoogde oxytocinewaarden bij hond en baas als die elkaar aankijken. Vermoedelijk heeft de hond zich aangepast aan de menselijke manier van oogcontact maken.

<b>Liefde </b>

In 1989 vroegen onderzoekers vreemdelingen om elkaar twee minuten aan te kijken. Na afloop bleken de proefpersonen meer liefde voor elkaar te voelen dan proefpersonen die elkaar niet hadden aangekeken.

Of alleen oogcontact al tot verliefdheid kan leiden? Dat is nog onduidelijk. Wel maken verliefde stellen veel oogcontact, terwijl – zo blijkt uit Amerikaans onderzoek – mensen die lust voelen voor een ander hun blik meer laten dwalen over diens gezicht en torso.

<b>Emotie </b>

Licht, medicijnen en drugs kunnen invloed hebben op de grootte van onze pupillen, dat is bekend. Uit allerlei onderzoeken blijkt dat ook emoties als angst, boosheid en geluk de pupillen beïnvloeden: ze worden er groter van. Alleen bij walging lijkt het alsof we ons willen onttrekken aan wat we zien en worden onze pupillen kleiner.

Onbewust vinden we grote pupillen aantrekkelijk. Combineer dat met het gegeven dat pupillen groter worden bij positieve gevoelens – en dan is het misschien wel logisch dat verliefden elkaar graag eindeloos in de ogen staren?

<b>Wegkijkers</b>

Kinderen met autisme kijken een ander tijdens het praten niet altijd aan. Vermoedelijk komt dit doordat emoties die opkomen tijdens zo’n gesprek veel vragen van hun werkgeheugen. Wanneer ze dan ook nog verbinding met de ander moeten maken via oogcontact, raakt dat werkgeheugen overbelast. De blik afwenden is dus een natuurlijke manier om de belasting te verminderen. Om toch wat informatie te krijgen over hun gesprekspartner kijken kinderen met autisme vaker naar diens mond.

Bron tekst: <link="https://bit.ly/2qpgIOB"><color=#075e36><u> "Kijken in de ogen is kijken in de ziel. Wat kunnen we bij elkaar aflezen?" Via Psychologie Magazine </u></color></link> 

Bron afbeelding: Mona Lisa, een schilderij van Leonardo DaVinci.', 'images/quests/687px-Mona_Lisa_by_Leonardo_da_Vinci_from_C2RMF_retouched.jpg', 15, true, 'default question', 4, false);
INSERT INTO public.ninjaapp_quest_page VALUES (52, 'Ninja Nu', false, 'Waarom deze quest?', 'Waar ben jij op dit moment in gedachten mee bezig? 

Denk je aan plannen voor de toekomst? Aan wat je wilt bereiken? Of denkt moet te bereiken? 

Verlang je naar een relatie? Een kind? Een eigen huis? Denk je aan de dag dat je écht meer gaat sporten? De dag dat je eindelijk een hogere functie bekleed? 

<i>Of sta je stil bij wat er hier en nu wél is?</i>

<b>De ''als, dan''-gedachten</b>

We hebben ze allemaal (wel eens): de ''als, dan''-gedachten. "Als ik een relatie heb, dan ben ik echt gelukkig" of "Als ik veel geld verdien of win, dan kan ik echt gaan doen wat ik zelf wil." We stellen daarmee ons geluk als het ware uit. De gedachten impliceren namelijk dat er nu eigenlijk iets anders zou moeten zijn. 

Mark Verhees, auteur van het boek ''Vóór positiviteit'' schrijft: “Sta vaak genoeg stil en waardeer wat er is. Focus daar op. Hoe meer geluksmomenten je ervaart tijdens je reis, hoe gelukkiger je er aan het einde van je leven op terug kunt kijken. Zo simpel is het. Geniet van je leven terwijl je het leeft”.

Met het doen van deze quest kun je ervaren wat er in het hier en nu is, los van bijvoorbeeld die relatie of dat vele geld. Welke mooie momenten beleef je in een week, terwijl je niet denkt aan dat wat er (nog) niet is? Hoe erg zou het zijn als dit verlangen er over een jaar nog niet is? 

> Om in beweging te komen is het van belang om stil te durven staan bij hoe het NU is. Dit kan spannend zijn als je voelt dat het niet OK gaat met je. Wel is het zo dat echte bewustwording de eerste stap is om je huidige denkpatroon op ten duur te veranderen.', 'default_images/default_quest.png', 19, true, 'default question', 4, false);
INSERT INTO public.ninjaapp_quest_page VALUES (54, '"spetter spatter spater"', false, '', 'De komende week is iedere ochtend (of avond) als je doucht jouw opdracht: <b>het ervaren van het water</b>. Neem waar hoe het voelt om water op je lijf te krijgen. Ontdek of je in gedachten bent. Heb je door dat je gedachten afdwalen? Verplaats dan je aandacht weer naar hoe het voelt om het water over je lijf heen te krijgen.', 'default_images/default_quest.png', 20, false, 'Hoe voelt het water?', 2, false);
INSERT INTO public.ninjaapp_quest_page VALUES (55, '"spetter spatter spater"', false, '', 'Hoe voelt het water? Hoe klinkt het water? Hoe ruikt het water? Hoe ziet het water eruit? Hoe proeft het water? Train deze week - en als je wilt langer - je zintuigen onder de douche.', 'default_images/default_quest.png', 20, false, 'Wat is jouw sterkste zintuig?', 3, false);
INSERT INTO public.ninjaapp_quest_page VALUES (57, 'Ninja Balans', false, '', 'Probeer deze week minimaal drie keer zo lang mogelijk te balanseren. <b>Hoe lang houd je dit vol?</b> Vind je het moeilijk? En merk je dat je er na wat oefening al beter in wordt? Je kunt deze oefening zo vaak doen als je wilt.', 'default_images/default_quest.png', 21, false, 'Wat denkt jij dat de sleutel is tot evenwicht?', 2, false);
INSERT INTO public.ninjaapp_quest_page VALUES (58, 'Ninja Balans', false, '', 'De sleutel tot een goed evenwicht, is een <b>goede ademhaling</b>. Let tijdens het balanseren op je ademhaling. Adem rustig in en uit.', 'default_images/default_quest.png', 21, false, 'Helpt een goede ademhaling je?', 3, false);
INSERT INTO public.ninjaapp_quest_page VALUES (59, 'Ninja Balans', false, 'Waarom deze quest?', '''Een goede balans is de sleutel tot..'' eigenlijk alles. Dat weten we en daar zijn we vaak op verschillende gebieden naar op zoek. 

In deze quest ervaar je hoe het gesteld is met jouw lichamelijk evenwicht en hoe bewustzijn (in dit geval het bewust zijn van je ademhaling) je kan helpen bij het houden van balans. 

Kijk eens of je deze ervaring kunt vertalen naar andere gebieden in je leven waarin je de balans zoekt.', 'default_images/default_quest.png', 21, true, 'default question', 4, false);
INSERT INTO public.ninjaapp_quest_page VALUES (61, 'Sensei Jouw lichaam', false, '', 'In School for Ninja heb je een persoonlijke Sensei. Een coach die jou op weg kan helpen naar een bewuster leven, wanneer je er zelf even niet uitkomt. Heb je al met hem of haar gechat? In deze quest wijzen we je graag op <b>een nog belangrijkere Sensei: jouw eigen lichaam</b>.', 'default_images/default_quest.png', 22, false, 'Luister jij vaak naar je lichaam?', 1, false);
INSERT INTO public.ninjaapp_quest_page VALUES (62, 'Sensei Jouw lichaam', false, '', 'Mensen die stressklachten hebben proberen hier vaak mee te copen. Dat wil zeggen: de stress reactie zo snel mogelijk laten verdwijnen. Bij een paniekaanval denken ze bijvoorbeeld aan hun voeten zodat de paniek zakt.', 'default_images/default_quest.png', 22, false, 'Los je hiermee op lange termijn de stressklachten op?', 2, false);
INSERT INTO public.ninjaapp_quest_page VALUES (63, 'Sensei Jouw lichaam', false, '', 'Deze week kun je proberen om, in plaats van afleiding te zoeken, jouw klachten welkom te heten. <b>Verwelkom</b> jouw paniek / snelle ademhaling / oorsuizen of andere klacht. Luister er naar. Stel er vragen aan. Hoe voelt jouw klacht precies? Waar komt hij vandaan? Als er vervelende gevoelens opkomen kun je deze altijd met jouw coach in de chat bespreken.', 'default_images/default_quest.png', 22, false, 'Wat leerde je door te luisteren naar jouw stressklacht?', 3, false);
INSERT INTO public.ninjaapp_quest_page VALUES (65, 'Wonderland', false, '', '"Elk avontuur heeft een eerste stap nodig" Is een quote van de Cheshire Cat uit Alice in Wonderland.  <b>Net als Alice ga jij in deze quest op avontuur</b>.', 'default_images/default_quest.png', 23, false, 'Ervaar je jouw leven als avontuurlijk?', 1, false);
INSERT INTO public.ninjaapp_quest_page VALUES (56, 'Ninja Balans', false, '', 'Ninja staan goed in contact met hun lichaam en mede daardoor zijn ze sterk in <b>evenwicht houden</b>. In deze quest ga jij dit ook oefenen. Zoek een plek en een item om deze week minimaal drie keer, zo lang mogelijk, op te balanseren. Bijvoorbeeld een paaltje, boomstam, speeltoestel of slackline. Ook thuis kun je je balans trainen, door met je ogen dicht op één been te gaan staan. Zorg in ieder geval dat je niet hard kunt vallen.', 'default_images/default_quest.png', 21, false, 'Wat wordt jouw balans plek / item?', 1, false);
INSERT INTO public.ninjaapp_quest_page VALUES (66, 'Wonderland', false, '', 'In deze Ninja opdracht ga je <b>een bekende of onbekende schaduwen</b>. Je infiltreert in iemands leven, voor een paar minuten. Volg deze week iemand ongemerkt; op kantoor, in de supermarkt, onderweg, waar dan ook. Doe het zolang je je niet te ongemakkelijk voelt.', 'default_images/default_quest.png', 23, false, 'Maakt hij / zij andere keuzes dan jij?', 2, false);
INSERT INTO public.ninjaapp_quest_page VALUES (67, 'Wonderland', false, '', 'Doe dit nog een aantal keer deze week, bij verschillende personen. Zo kom je misschien op plekken waar je nooit eerder kwam en leer je zaken kennen die je nooit eerder zag.', 'default_images/default_quest.png', 23, false, 'Wat was jouw mooiste avontuur deze week?', 3, false);
INSERT INTO public.ninjaapp_quest_page VALUES (60, '"spetter spatter spater"', false, 'Waarom deze quest?', 'Gemiddeld hebben we zo''n 50.000 gedachten per dag. Van een groot deel daarvan zijn we ons vaak niet bewust. Veel dingen doen we namelijk op automatische piloot. Tandenpoetsen, eten, douchen.. maar ook tijdens een dagelijkse fiets of auto route letten we vaak nauwelijks op de omgeving. 

Op die momenten dwalen onze gedachten af naar dingen die ons bezighouden. We gaan onze to-do lijstjes af, vragen ons af waarom iets niet gegaan is zoals we dat hadden gewild of vormen angsten voor wat nog mis kán gaan (zoals die belangrijke presentatie die je die dag moet houden). 

Door onze gedachten zo te laten afdwalen, geven we veel energie uit zonder dat in de gaten te hebben. Ook ‘verdwijnen’ we op die manier uit het hier & nu.

<b>De quest "Spetter spatter spater" helpt je om meer in het hier & nu te zijn en minder energie te verspillen aan talloze gedachten, door het douchen bewust te ervaren.</b>

Je kunt deze oefening vergelijken met bekende oefeningen binnen ''Mindfulness''. Bewust zijn van je gedachten en oefenen om meer te focussen op het hier & nu kan zorgen voor minder ruimte voor piekeren. Door je volle aandacht te schenken aan je ervaringen worden ze levendiger en intenser. 

"Wie aandachtig leeft, heeft meer oog voor de kleine dingen, een fluitende vogel, een opkomende zon, een mooie boom, de warmte van een douche. Het huidige moment heeft veel meer te bieden dan je denkt." - Anja Uitdewilligen Mindfulness trainer', 'default_images/default_quest.png', 20, true, 'default question', 0, false);
INSERT INTO public.ninjaapp_quest_page VALUES (53, '"spetter spatter spater"', false, '', 'Een Ninja gebruikt zijn of haar <b>zintuigen</b> om goede keuzes te maken. Onderdeel van deze ''School'' is dan ook het trainen van je zintuigen. De komende week ga je dit doen tijdens het <b>douchen</b>.', 'default_images/default_quest.png', 20, false, 'Waar denk jij meestal aan onder de douche?', 1, false);
INSERT INTO public.ninjaapp_quest_page VALUES (64, 'Sensei Jouw lichaam', false, 'Waarom deze quest?', '''Gezond zijn'' wil niet zeggen dat je nooit ziek of nooit moe bent. Of dat je nooit een pijntje of kwaaltje mag hebben. Integendeel! <b>Lichamelijke sensaties zijn de boodschappers van jouw lichaam die je laten merken dat er iets aandacht verdient</b>. Gevoelens van honger en dorst zijn herkenbare voorbeelden. Hebben we dorst dan gaan we drinken en honger stimuleert ons om te gaan eten. 

Vagere signalen zijn echter soms moeilijker te duiden. De nodige actie is dan vaak ook niet direct in te schatten. Wat verwacht ons lichaam van ons bij een paniekaanval? Of en hoge hartslag? 

In de quest ''Sensei Jouw Lichaam'' oefen je het stilstaan bij de boodschap die jouw lichaam wil overbrengen. Waar komt de sensatie vandaan? Wanneer speelt deze sensatie op? 

<b>Door je sensaties te verwelkomen en er aandacht aan te geven, kun je meer leren over wat je lichaam je probeert te vertellen.</b>


✭ Veel vage klachten die we ervaren kunnen duiden op het ervaren van een grote hoeveelheid stress. 
O.a. op <link="https://bit.ly/2JCZGnh"><color=#075e36><u>Thuisarts.nl</u></color></link> en <link="https://bit.ly/2XClANq"><color=#075e36><u>Newsner.com</u></color></link> lees je meer over de symptomen van stress en overspannenheid. 

<i>Bij ernstige klachten raden we je aan contact op te nemen met je huisarts.</i>', 'default_images/default_quest.png', 22, true, 'default question', 4, false);
INSERT INTO public.ninjaapp_quest_page VALUES (77, 'Ted talks', false, 'Waarom deze quest?', 'In deze tweede fase van School for Ninja <b>''Sensatie en Inspiratie''</b> ga je op ontdekkingstocht. Je neemt bewust waar, met al je zintuigen, wat er is in het hier & nu. Het bekende en onbekende bewust ervaren. In deze quest laat je je inspireren door beschikbare bronnen die er zijn om je kijk op aanwezige mogelijkheden te verbreden. 

Zoals je weet zijn de fases in School for Ninja gebaseerd op Theory U. Je leest  <link="https://www.theoryuplein.nl/theory-u/"><color=#075e36><u>hier</u></color></link> meer over deze methode voor verandermanagement. 

<b>Over TED:</b> In 1984 bedacht Amerikaanse architect en grafisch ontwerper Richard Saul Wurman dat technologie, entertainment en design steeds meer naar elkaar toegroeide en wilde hij hierover een jaarlijks evenement organiseren. Vooraanstaande mensen mochten in 18 minuten de presentatie van hun leven geven over hun expertise, om deze inspirerende ideeën te verspreiden. Tegenwoordig staan er vele duizenden Ted talks van sprekers over de hele wereld op prijswinnend platform Ted.com.', 'default_images/default_quest.png', 25, true, 'default question', 4, false);
INSERT INTO public.ninjaapp_quest_page VALUES (85, 'JOMO', false, 'Waarom deze quest?', 'In deze tweede fase van School for Ninja <b>''Sensatie en Inspiratie''</b> ga je op ontdekkingstocht. Je neemt bewust waar, met al je zintuigen, wat er is in het hier & nu. Je gaat het bekende en onbekende bewust ervaren. In deze quest kijk je naar de mogelijkheden die er zijn in het hier & nu, in plaats van te focussen op de dingen die je mogelijk mist.

Veel bang zijn dat je iets mist wordt ook wel ''FOMO'' genoemd. De Fear Of Missing Out. Omdat er voor ons nu heel veel mogelijk is, komt FOMO erg veel voor. Psychiater Dirk de Wachter noemt het dé ziekte van deze tijd. Hij stelt dat we geobsedeerd zijn door geluk en dat de obsessie met het maakbare geluk de angst voedt om iets te missen. En dat bederft uiteindelijk juist ons geluksgevoel. 

Toch is FOMO niet alleen maar negatief, want nieuwsgierig zijn en je willen ontwikkelen op een breed vlak is natuurlijk alleen maar goed. <b> De sleutel hierbij is - wederom - balans</b>. Die balans kun je creëren door wat meer JOMO te ontwikkelen. De Joy Of Missing Out: je bewust ''vervelen''. 

Als je gewend bent om je hele leven vol te plannen kunnen ''verveel'' momenten onwennig aanvoelen, maar als je doorzet kun je er van gaan genieten. Probeer je te focussen op dat wat er nu is en niet op wat je mist. Kijk bijvoorbeeld niet op sociale media naar wat je vrienden allemaal aan het doen zijn terwijl jij je JOMO moment ervaart. 

<b>Het is een groot geluk als je tijd alleen kunt doorbrengen zonder angstig te worden voor hetgeen dat je mogelijk mist</b>. Door meer JOMO-momenten aan je leven toe te voegen, ontstaat er rust en ruimte voor jezelf. De ruimte om te reflecteren. Hoe voel jij je nu? Wat vind je echt belangrijk? Waar wil je je tijd aan besteden? 

Minder doen met meer kwaliteit! 

Tips voor meer JOMO in je leven vind je <link="https://holistik.nl/joy-of-missing-out/"><color=#075e36><u>hier</u></color></link>.', 'default_images/default_quest.png', 27, true, 'default question', 4, false);
INSERT INTO public.ninjaapp_quest_page VALUES (81, 'Chloe talks', true, 'Waarom deze quest?', 'Zoals de quote van De Griekse filosoof Heraclitus gelooft School for Ninja dat verandering is de enige constante is. Een wereld die altijd in verandering is, vraagt om een aanpassingsvermogen. Aanpassen is de kern van evolutie. De mens is dan ook zo heersend op onze wereld, omdat we over een goed aanpassingsvermogen beschikken. 

Tegelijkertijd is dit aanpassingsvermogen een reden voor het ontstaan van stress, want als je het idee hebt dat je je moet aanpassen aan iets dat je aanpassingsvermogen te boven dreigt te gaan, ervaar je stress. De vraag rest dus: waaraan pas je je aan? en waaraan niet? 

Eén van de veranderingen die nu heel relevant is, is de ontwikkeling van techniek. Volgens <link="https://nl.wikipedia.org/wiki/Wet_van_Moore"><color=#075e36><u>''de wet van Moore''</u></color></link>  stijgt onze technische capiciteit exponentieel. Dit betekent dat techniek zich steeds sneller ontwikkeld. In de bovenstaande afbeelding zie je dat techniek op het punt staat ons mensen in te halen op het gebied van intilligentie. 

Wil jij in de toekomst zingevend werk hebben? Dan kun je je dus afvragen: Hoe pas ik me aan, als techniek mijn producerend en berekenend vermogen uit handen neemt?

Misschien is het niet zo dat robots en A.I. leiden tot Cloe''s, misschien worden onze banen (voorlopig) niet overgenomen. Toch denken we dat het ook dan waardevol is om je te realiseren dat aanpassingsvermogen waardevol is. Wanneer jouw trein-dienstregeling veranderd, of er een re-organisatie is op werk, kan je hier flexibel, speels mee om gaan. Je weet waaraan je je wilt aanpassen en waaraan niet. Je kunt dan dicht bij jezelf blijven en toch meebewegen met een omgeving die altijd in verandering is.', 'images/quests/Schermafbeelding_2019-04-24_om_11.26.47.jpg', 26, true, 'default question', 4, false);
INSERT INTO public.ninjaapp_quest_page VALUES (68, 'Wonderland', true, 'Waarom deze quest?', 'In deze tweede fase van School for Ninja <b>''Sensatie en Inspiratie''</b> ga je op ontdekkingstocht. 

In de quest ''Wonderland'' laat je je inspireren door de levens van anderen. 

Welke boodschappen iemand anders? Waar fietst iemand anders naartoe? 

Een avontuurlijke, speelse manier om andere patronen of leefregels te leren kennen. 

Vind je het spannend om deze opdracht te doen? Goed juist! Probeer bewust te ervaren of je in je lichaam merkt hoever je in deze opdracht kunt gaan. Wat voelt goed voor jou? En wat niet? Houd daarbij ook rekening met wat voor een ander onprettig kan zijn.', 'images/quests/alice-in-wonderland-quote.jpg', 23, true, 'default question', 4, false);
INSERT INTO public.ninjaapp_quest_page VALUES (73, 'Wie niet waagt...', true, 'Waarom deze quest?', 'In deze quest bestuderen we het <b>Learning Zone model</b> van Senninger. In dit model worden fases van spanning opgedeeld in drie zones; de Comfort Zone, de Learning Zone en de Panic Zone.

<b> Bewust worden van in welke zone je bevindt, kan je helpen om te herkennen of jij meer of juist minder uitdaging nodig hebt.</b> Wanneer je je in welke zone bevindt is voor iedereen anders. Voor jou kan het geven van een presentatie bijvoorbeeld in de Comfort Zone liggen, terwijl iemand anders dat eerder onder de Panic Zone zou scharen. 

De <i>Learning Zone </i> uit het Learning Zone model kun je vergelijken met het gevoel van <b> ''in de Flow zitten''</b> (zie afbeelding). 

Amerikaans/Hongaarse psycholoog Mihaly Csikszentmihaly deed in de jaren ’70 en ’80 onder andere onderzoek naar naar de factoren die er voor zorgen dat je tijdens het uitvoeren van een taak of activiteit gelukkig bent. Hij concludeerde dat de wegingsfactoren “uitdaging” (challenge level) en “vaardigheid” (skill level), die tegen elkaar worden uitgezet, hierbij bepalend zijn: iemand is het meest gelukkig als zijn vaardigheden tijdens het uitvoeren van een taak of activiteit op hoog niveau worden aangesproken. 

Wil je meer informatie over Flow? Wessel Peeters vatte Flow Theorie samen op <link="https://bit.ly/2DsIXB5"><color=#285E38><u>Vernieuwenderwijs.nl</color></u></link>, je vindt daar ook de video waarin Mihaly Csikszentmihaly zelf meer verteld over Flow. 


De zones nog een keer op een rijtje: 
<b>De Comfort Zone</b>: Het is belangrijk dat deze zone aanwezig is in je leven. Een plek om terug te kunnen keren om te reflecteren, zaken weer in perspectief te kunnen zetten en op te kunnen laden. Maar, teveel in deze Comfort Zone blijven zitten, is niet bevorderlijk voor je groei. Bovendien kan te weinig spanning een ''bore-out'' - het tegenovergestelde van een burn-out - veroorzaken. 

<b> De Learning Zone </b>: Deze zone ligt net buiten je veilige haven. Dit is het gebied waarbinnen je jouw nieuwsgierigheid kunt volgen en nieuwe ontdekkingen kunt doen. Experimenteren met nieuwe mogelijkheden en je grenzen verkennen. Hier kun je gaan leren en groeien! 

<b> De Panic Zone </b>: In de Panic Zone overheerst een gevoel van angst. In deze zone wordt er teveel van je gevraagd en/of vraag je teveel van jezelf.', 'images/quests/Schermafbeelding_2019-04-24_om_16.24.43.jpg', 24, true, 'default question', 4, false);


--
-- Name: ninjaapp_quest_page_id_seq; Type: SEQUENCE SET; Schema: public; Owner: radicalg
--

SELECT pg_catalog.setval('public.ninjaapp_quest_page_id_seq', 85, true);


--
-- PostgreSQL database dump complete
--

