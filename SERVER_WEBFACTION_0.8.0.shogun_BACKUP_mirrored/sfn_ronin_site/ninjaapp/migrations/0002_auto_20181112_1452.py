# -*- coding: utf-8 -*-
# Generated by Django 1.11.16 on 2018-11-12 14:52
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0008_alter_user_username_max_length'),
        ('ninjaapp', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Organization_User',
            fields=[
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, primary_key=True, serialize=False, to=settings.AUTH_USER_MODEL)),
                ('is_in_organization', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ninjaapp.Organization')),
            ],
        ),
        migrations.CreateModel(
            name='Post_Log',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('privacy', models.CharField(choices=[('everyone', 'Everyone'), ('friends', 'Friends'), ('group_members', 'Group members'), ('friends_and_group', 'Friends and Group members'), ('only_me', 'Only me')], max_length=20)),
            ],
        ),
        migrations.AddField(
            model_name='coach',
            name='has_quests',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='ninjaapp.Quest'),
        ),
        migrations.AddField(
            model_name='ninja',
            name='has_quests',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='ninjaapp.Quest'),
        ),
        migrations.AddField(
            model_name='quest_status',
            name='started',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='quest',
            name='creation_date',
            field=models.DateTimeField(auto_now_add=True, verbose_name='date created'),
        ),
        migrations.AlterField(
            model_name='quest_status',
            name='started_on_date',
            field=models.DateTimeField(auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='quest_status',
            name='unlocked',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='post_log',
            name='belongs_to_ninja',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ninjaapp.Ninja'),
        ),
        migrations.AddField(
            model_name='post_log',
            name='belongs_to_quest',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ninjaapp.Quest'),
        ),
    ]
