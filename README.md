# schoolforninja_server
Server for School for Ninja


Requirements
============
Use:

pip install -r \Github\schoolforninja_server\webfaction_schoolforninja\sfn_ronin_site\ronin_req.txt



Settings
========

\Github\schoolforninja_server\webfaction_schoolforninja\sfn_ronin_site\sfn_ronin_site\settings
- base.py
- your_local_settings.py (use the name that you like...)
(Create a local .bat to run your local settings: )
leunam_runserver.bat (check it as example)


- Settings On server: settings_FOR_SERVER.noexe (change it to settings.py to run it on server...)

Note: On current SERVER (on Webfaction) libraries are installed on the user, the env libraries are NOT USED! (even if they're installed)

Downloads
=========

Postgres tool (for your local database)
https://www.openscg.com/bigsql/oscg_download/?file=packages/PostgreSQL-9.5.15-1-win64-bigsql.exe&user=${auth.authName}



DATABASE DUMPS
==============
TO SAVE DATA
manage.py dumpdata > file.json


TO LOAD DATA (Beware if you have the DB with stuff you need to clean it up with: manage.py flush)
python manage.py loaddata --settings=sfn_ronin_site.settings.yourlocalsettings --exclude auth.permission --exclude contenttypes <PATHTO>/DB_DUMP.json

**API STUFF**
=========


API URL views
=============

For token generation:


=============

Register
Method: POST
Endpoint: /rest-auth/registration/
Payload:

{
    "username": "USERNAME",
    "password1": "PASSWORD",
    "password2": "PASSWORD",
    "email": "OPTIONAL_EMAIL"
}

___
Login
Method: POST
Endpoint: /rest-auth/login/
Payload:

{
    "username": "USERNAME",
    "password": "PASSWORD"
}

___
Logout
Method: POST
Endpoint: /rest-auth/logout/
Headers: Authorization: JWT YOUR_TOKEN_HERE
___	
Refresh Token
Method: POST
Endpoint: /refresh-token/
Payload:

{
    "token": "YOUR_OLD_TOKEN"
}


JWT guidelines
==============
https://getblimp.github.io/django-rest-framework-jwt/#usage
