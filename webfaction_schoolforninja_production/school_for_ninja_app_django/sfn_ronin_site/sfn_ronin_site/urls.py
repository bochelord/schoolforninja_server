"""sfn_ronin_site URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views as auth_views

from ninjaapp import views
from ninjaapp.views import ServerStatusViewSet,Phase_StatusViewSet,ChatHistoryViewSet,SurveyQuestionViewSet,SurveyAnswersViewSet,password_reset, QuestPagesViewSet,DraftViewSet, signup_user, UserViewSet, NinjaViewSet, CoachViewSet, OrganizationViewSet, QuestViewSet, Quest_StatusViewSet, Post_LogViewSet, BadgeViewSet, PhaseViewSet, ActionViewSet

from postman import urls as postman

from rest_framework import routers
from rest_framework_jwt.views import refresh_jwt_token, verify_jwt_token

# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'ninjas',NinjaViewSet)
router.register(r'coaches',CoachViewSet)
router.register(r'organizations',OrganizationViewSet)
router.register(r'quests',QuestViewSet)
router.register(r'quest_statuses',Quest_StatusViewSet)
router.register(r'quest_pages', QuestPagesViewSet)
router.register(r'post_logs',Post_LogViewSet)
router.register(r'badge', BadgeViewSet)
router.register(r'phase', PhaseViewSet)
router.register(r'action', ActionViewSet)
router.register(r'drafts',DraftViewSet)
router.register(r'survey_answers',SurveyAnswersViewSet)
router.register(r'survey_questions',SurveyQuestionViewSet)
router.register(r'chat_history', ChatHistoryViewSet)
router.register(r'phase_status', Phase_StatusViewSet)
router.register(r'server_status', ServerStatusViewSet)
#router.register(r'signup', SignupViewSet)

urlpatterns = [
    url(r'^admin/', admin.site.urls), #admin urls
    url('accounts/', include('django.contrib.auth.urls')), #account creation (register, login) for 
    url(r'', include('ninjaapp.urls')),
    url(r'^messages/', include('postman.urls', namespace='postman', app_name='postman')),
    url(r'^ninjapi/', include(router.urls)),
    url(r'^ninjapi/api-auth/', include('rest_framework.urls')),
    url(r'^rest-auth/', include('rest_auth.urls')),
    url(r'^rest-auth/registration/', include('rest_auth.registration.urls')),
    url(r'^refresh-token/', refresh_jwt_token),
    url(r'^rest-auth/verify-token/', verify_jwt_token),
    url(r'^ninjapi/signup/', signup_user),
    # url(r'^password_reset/$', auth_views.password_reset, name='password_reset'),
    url(r'^password_reset/$', password_reset),
    url(r'^password_reset/done/$', auth_views.password_reset_done, name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.password_reset_confirm, name='password_reset_confirm'),
    url(r'^reset/done/$', auth_views.password_reset_complete, name='password_reset_complete'),
]


urlpatterns += static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)