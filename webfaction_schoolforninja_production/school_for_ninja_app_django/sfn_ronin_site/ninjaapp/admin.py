from django.contrib import admin
#import models
from ninjaapp.models import ServerStatus,Phase_Status,ChatHistory, SurveyQuestion,SurveyAnswers,Draft,Coach, Organization, Ninja, Quest, Quest_Status, Organization_User, Post_Log, Badge, Phase, Quest_Page

# Register your models here.
admin.site.register(Coach)
admin.site.register(Organization)
admin.site.register(Ninja)
admin.site.register(Quest)
admin.site.register(Quest_Status)
admin.site.register(Organization_User)
admin.site.register(Post_Log)
admin.site.register(Badge)
admin.site.register(Phase)
admin.site.register(Quest_Page)
admin.site.register(Draft)
admin.site.register(SurveyAnswers)
admin.site.register(SurveyQuestion)
admin.site.register(ChatHistory)
admin.site.register(Phase_Status)
admin.site.register(ServerStatus)

list_display= ('image_img','product',)
readonly_fields = ('image_img',)

fields = ( 'image_tag', )
readonly_fields = ('image_tag',)