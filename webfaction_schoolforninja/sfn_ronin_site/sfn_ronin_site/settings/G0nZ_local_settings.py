from .base import *

ALLOWED_HOSTS = ['127.0.0.1', 'localhost']

DEBUG = True



STATIC_ROOT = 'C:/Users/G0nZ/Documents/GitHub/schoolforninja_server/webfaction_schoolforninja/sfn_ronin_site/static'

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.sqlite3', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
#         'NAME': os.path.join(SITE_ROOT, 'db.sqlite3'),   # Or path to database file if using sqlite3.
#     }
# }

STATICFILES_DIRS = (
#     # Put strings here, like "/home/html/static" or "C:/www/django/static".
#     # Always use forward slashes, even on Windows.
#     # Don't forget to use absolute paths, not relative paths.
#'C:/Github/schoolforninja_SERVER/webfaction_schoolforninja/sfn_ronin_site/static/',
    os.path.join(BASE_DIR, 'static/'), # build appropriate path
#     #os.path.join(os.path.dirname(__file__),'media').replace('\\','/'),
#MEDIA_ROOT = os.path.join(BASE_DIR, 'media/')
 )


DATABASES = {
    'default': {
        'CONN_MAX_AGE': 0,
        'ENGINE': 'django.db.backends.postgresql_psycopg2', 
        'NAME': 'postgres',
        'USER': 'postgres',
        'PASSWORD': 'ksironline.com1',
        'HOST': '127.0.0.1',   # Or an IP Address that your DB is hosted on
        'PORT': '5432',
    }
}