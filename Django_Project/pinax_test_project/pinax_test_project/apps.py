from importlib import import_module

from django.apps import AppConfig as BaseAppConfig


class AppConfig(BaseAppConfig):

    name = "pinax_test_project"

    def ready(self):
        import_module("pinax_test_project.receivers")
