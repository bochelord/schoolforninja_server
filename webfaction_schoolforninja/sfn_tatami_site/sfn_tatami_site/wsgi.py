"""
WSGI config for sfn_tatami_site project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/howto/deployment/wsgi/
"""

import os
import sys 

from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sfn_tatami_site.settings")

sys.path.insert(0, "/home/radicalg/webapps/school_for_ninja_app_django/env_tatami3.6/lib/python3.6/site-packages/djangocms_admin_style")
application = get_wsgi_application()
