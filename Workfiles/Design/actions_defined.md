============ List of Actions possible on School for Ninja

Register
Login
Start Quest (Internal)
Complete Quest (Internal)
Logout
Phase Start (Internal)
Add Coach
Remove Coach
Create Post Log
Delete Post
Edit Post
Create Quest
Edit Quest
Remove Quest
Activate Quest 
Deactivate Quest
Send Message
Receive Message
Rate Quest
Report User
Register Achievement (Internal)
Create Profile (Add Ninja)
Edit Profile
Delete Profile
Unlock Quest (Internal)
Change Profile Privacy
Add Draft (Post)
Edit Draft (Post)
Remove Draft (Post)
Report Issue (System)
Login Error
Login Password Reset