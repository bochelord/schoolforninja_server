
from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from ninjaapp.models import Ninja, Coach


class SignUpForm(UserCreationForm):

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2')

    def save(self, commit=True):
        user = super(SignUpForm,self).save(commit=False)
        user.email = self.cleaned_data.get('email')

        if commit:
            user.save()

        return user

class SignUpNinjaForm(forms.ModelForm):

    class Meta:
        model = Ninja
        # fields = ('ninja_name', 'avatar', 'gender')
        fields = ('ninja_name', 'gender')


class SignUpCoach(forms.ModelForm):

    class Meta:
        model = Coach
        fields = ('about_me' , 'gender')

# class PasswordResetForm(forms.Form):
#     email = forms.EmailField(label=("email"), max_length=254)

#     def get_users(self, email):
#         """Given an email, return matching user(s) who should receive a reset.
#         This allows subclasses to more easily customize the default policies
#         that prevent inactive users and users with unusable passwords from
#         resetting their password.
#         """
#         active_users = User._default_manager.filter(**{
#             '%s__iexact' % User.get_email_field_name(): email,
#             'is_active': True,
#         })
#         return (u for u in active_users if u.has_usable_password())