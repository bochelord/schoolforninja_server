from django.contrib import admin
#import models
from Ninja_App.models import Coach, Organization, Ninja, Quest, Quest_Status

# Register your models here.
admin.site.register(Coach)
admin.site.register(Organization)
admin.site.register(Ninja)
admin.site.register(Quest)
admin.site.register(Quest_Status)

fields = ( 'image_tag', )
readonly_fields = ('image_tag',)