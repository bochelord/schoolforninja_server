###########################
# Serializers for NINJAPI
#
###########################
#
# (c) 2018 School for Ninja
#
###########################

from django.contrib.auth.models import User
from rest_framework import serializers

from ninjaapp.models import ServerStatus, Phase_Status,ChatHistory, Ninja, Coach, Organization, Quest, Quest_Status, Post_Log, Badge, Phase, Quest_Page, Draft, SurveyAnswers,SurveyQuestion


from actstream.models import Action

from drf_extra_fields.fields import Base64ImageField
## Auth serializers


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        #fields = '__all__'
        #fields = ('url', 'username', 'email', 'groups', 'password')
        fields = ('url','username', 'email', 'password')

    def create(self, validated_data, instance=None):
        user = User.objects.create(**validated_data)
        user.set_password(validated_data['password'])
        user.save()
        print("User is being created on the UserSerializer. User: " + user.username)

        print("Starting the process of creating anew NINJA with - userpk: " + str(user.id) )
        numerodeninjas = Ninja.objects.all().count()

        print('NumerodeNinjas' + str(numerodeninjas))
        ninja = Ninja.objects.create(user=user,ninja_name="nieuwe_ninja_"+str(numerodeninjas+1),gender="m",domain="default",power="default")
        #ninja.user = user

        #ninja.gender = "m"
        #ninja.domain = "undefined"
        #ninja.power = "undefined"
        ninja.save()

        return user



## Ninjaapp serializers
class NinjaSerializer(serializers.HyperlinkedModelSerializer):
    avatar = Base64ImageField(required=False)
    #user = UserSerializer()
    class Meta:
        model = Ninja
        fields = '__all__'
        #fields = ('user','url', 'ninja_name', 'avatar','gender', 'level', 'domain', 'power', 'has_friends', 'has_coaches', 'has_quests')
    # def create(self,validated_data,instance=None):
    #     print("Starting the process of creating anew NINJA with - userpk: " + validated_data['user'] )
    #     ninja = Ninja.objects.create(**validated_data)
    #     ninja.user = validated_data['user']
    #     ninja.gender = validated_data['gender']
    #     ninja.domain = validated_data['domain']
    #     ninja.power = validated_data['power']
    #     ninja.save()
    #     return ninja

class CoachSerializer(serializers.HyperlinkedModelSerializer):
    coach_avatar = Base64ImageField(required=False)
    class Meta:
        model = Coach
        fields = '__all__'

class OrganizationSerializer(serializers.HyperlinkedModelSerializer):
    icon = Base64ImageField(required=False)
    class Meta:
        model = Organization
        fields = '__all__'

class Quest_PageSerializer(serializers.ModelSerializer):
    image = Base64ImageField(required=False)
    class Meta:
        model = Quest_Page
        #fields = ('has_image','title_text','description_text','image','quest_title')
        fields = '__all__'
class DraftSerializer(serializers.HyperlinkedModelSerializer):
    image = Base64ImageField(required=False)
    class Meta:
        model = Draft
        fields = '__all__'

class QuestSerializer(serializers.HyperlinkedModelSerializer):
    quest_image = Base64ImageField(required=False)
    quest_cover_image = Base64ImageField(required=False)
    class Meta:
        model = Quest
        #fields = ('url','quest_title','has_pages',)
        fields = '__all__'

class Quest_StatusSerializer(serializers.HyperlinkedModelSerializer):
    #has_drafts = DraftSerializer(many=True)
    class Meta:
        model = Quest_Status
        fields = '__all__'

class Post_LogSerializer(serializers.HyperlinkedModelSerializer):
    image1 = Base64ImageField(required=False)
    image2 = Base64ImageField(required=False)
    image3 = Base64ImageField(required=False)
    image4 = Base64ImageField(required=False)
    image5 = Base64ImageField(required=False)
    story_cover_image = Base64ImageField(required=False)
    class Meta:
        model = Post_Log
        fields = '__all__'
        #fields = ('id','url','postTitle','postContent','belongs_to_ninja','belongs_to_quest','timestamp','privacy')


class BadgeSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = Badge
		fields = ('url','name', 'description', 'image','unlocked_date')

class PhaseSerializer(serializers.HyperlinkedModelSerializer):
	class Meta(object):
		model = Phase
		fields = ('url','name', 'theme', 'info')

class Phase_StatusSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Phase_Status
        fields = '__all__'

class SurveyAnswersSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = SurveyAnswers
        fields = '__all__'

class SurveyQuestionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = SurveyQuestion
        fields = '__all__'
## Action serializers

class ActionSerializer(serializers.HyperlinkedModelSerializer):
	class Meta(object):
		model = Action
		fields = ('url','verb','actor_object_id','target_object_id','timestamp','description','action_type')

class ChatHistorySerializer(serializers.HyperlinkedModelSerializer):
    class Meta(object):
        model = ChatHistory
        fields = '__all__'

class ServerStatusSerializer(serializers.HyperlinkedModelSerializer):
    class Meta(object):
        model = ServerStatus
        fields = '__all__'