--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Data for Name: ninjaapp_quest; Type: TABLE DATA; Schema: public; Owner: radicalg
--

INSERT INTO public.ninjaapp_quest VALUES (11, '2019-01-24 09:52:23.235459+00', '2019-01-24', false, false, 1, 'Jouw Ninja Naam', 2, 'default_images/default_quest.png', false, 'default_images/default_quest.png', 'default text', 'Inspiratie, actief');
INSERT INTO public.ninjaapp_quest VALUES (13, '2019-01-25 10:36:59.569815+00', '2019-01-25', false, false, 1, 'Stoppen of doorrijden?', 2, 'default_images/default_quest.png', false, 'default_images/default_quest.png', 'default text', 'Inspiratie, actief');
INSERT INTO public.ninjaapp_quest VALUES (14, '2019-01-31 14:56:49.260956+00', '2019-01-31', false, false, 1, 'Donuts', 2, 'default_images/default_quest.png', false, 'default_images/default_quest.png', 'default text', 'Inspiratie, actief');
INSERT INTO public.ninjaapp_quest VALUES (12, '2019-01-25 10:36:22.037786+00', '2019-01-25', true, false, 1, 'Nulmeting', 3, 'default_images/default_quest.png', false, 'default_images/default_quest.png', 'default text', 'Inspiratie, actief');
INSERT INTO public.ninjaapp_quest VALUES (15, '2019-02-04 13:23:03.507579+00', '2019-02-04', true, false, 1, 'In jouw ogen', 2, 'default_images/default_quest.png', false, 'default_images/default_quest.png', 'default text', 'Inspiratie, actief');
INSERT INTO public.ninjaapp_quest VALUES (16, '2019-02-04 13:24:48.39307+00', '2019-02-04', true, false, 1, 'Vertragen', 4, 'default_images/default_quest.png', false, 'default_images/default_quest.png', 'default text', 'Inspiratie, actief');
INSERT INTO public.ninjaapp_quest VALUES (17, '2019-02-04 13:53:05.667213+00', '2019-02-04', true, false, 1, 'De teken-check', 2, 'default_images/default_quest.png', false, 'default_images/default_quest.png', 'default text', 'Inspiratie, actief');
INSERT INTO public.ninjaapp_quest VALUES (18, '2019-02-04 13:53:24.969729+00', '2019-02-04', false, false, 1, 'Het spelende kind', 2, 'default_images/default_quest.png', false, 'default_images/default_quest.png', 'default text', 'Inspiratie, actief');
INSERT INTO public.ninjaapp_quest VALUES (10, '2019-01-23 10:51:37.170752+00', '2019-01-23', false, false, 1, 'Hoe gaat het?', 2, 'images/quests/default_quest.png', false, 'default_images/default_quest.png', 'default text', 'Inspiratie, actief');
INSERT INTO public.ninjaapp_quest VALUES (19, '2019-03-08 10:26:13.180104+00', '2019-03-08', true, true, 1, 'Ninja Nu', 4, 'default_images/default_quest.png', false, 'default_images/default_quest.png', 'default text', 'Inspiratie, actief');
INSERT INTO public.ninjaapp_quest VALUES (20, '2019-04-15 12:12:46.587278+00', '2019-04-15', false, false, 2, '"spetter spatter spater"', 4, 'default_images/default_quest.png', false, 'default_images/default_quest.png', 'default text', 'Inspiratie, actief');
INSERT INTO public.ninjaapp_quest VALUES (21, '2019-04-15 12:13:24.789546+00', '2019-04-15', true, false, 2, 'Ninja Balans', 2, 'default_images/default_quest.png', false, 'default_images/default_quest.png', 'default text', 'Inspiratie, actief');
INSERT INTO public.ninjaapp_quest VALUES (22, '2019-04-15 12:13:47.061129+00', '2019-04-15', true, false, 2, 'Sensei Jouw lichaam', 2, 'default_images/default_quest.png', false, 'default_images/default_quest.png', 'default text', 'Inspiratie, actief');
INSERT INTO public.ninjaapp_quest VALUES (23, '2019-04-15 12:14:08.63301+00', '2019-04-15', false, false, 2, 'Wonderland', 2, 'default_images/default_quest.png', false, 'default_images/default_quest.png', 'default text', 'Inspiratie, actief');
INSERT INTO public.ninjaapp_quest VALUES (24, '2019-04-15 12:14:26.465996+00', '2019-04-15', true, false, 2, 'Wie niet waagt...', 2, 'default_images/default_quest.png', false, 'default_images/default_quest.png', 'default text', 'Inspiratie, actief');
INSERT INTO public.ninjaapp_quest VALUES (25, '2019-04-15 12:14:43.972675+00', '2019-04-15', true, false, 2, 'Ted talks', 2, 'default_images/default_quest.png', false, 'default_images/default_quest.png', 'default text', 'Inspiratie, actief');
INSERT INTO public.ninjaapp_quest VALUES (27, '2019-04-15 12:15:26.887064+00', '2019-04-15', true, false, 2, 'JOMO', 2, 'default_images/default_quest.png', false, 'default_images/default_quest.png', 'default text', 'Inspiratie, actief');
INSERT INTO public.ninjaapp_quest VALUES (26, '2019-04-15 12:15:04.796735+00', '2019-04-15', true, false, 2, 'Chloe talks', 2, 'default_images/default_quest.png', false, 'default_images/default_quest.png', 'default text', 'Inspiratie, actief');


--
-- Name: ninjaapp_quest_id_seq; Type: SEQUENCE SET; Schema: public; Owner: radicalg
--

SELECT pg_catalog.setval('public.ninjaapp_quest_id_seq', 27, true);


--
-- PostgreSQL database dump complete
--

